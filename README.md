# Kumparan Academy Big-Project

Back-end of Kumparan Academy Big-Project; consists of three main services: Article, Comment and User.
Served with GraphQL and interconnected with gRPC.

## Installing

This whole project has been containerized with Docker for the ease of using and installing.
To install this services, you can go to the project path and run:

```
docker-compose build
```

## Running

To run the services, easily run :

```
docker-compose up
```
 so the services and database will run on your machine

 ## Built With

 * Go - a programming language built by Google
 * Echo Framework - to serve our HTTP Server
 * go-graphql - to serve the GraphQL Server
 * gRPC with Protocol Buffer - a modern RPC platform to built a scalable and robust microservices
 * CockroachDB - basicly PostgreSQL; but distributed!

## Authors

This whole project was built by :
- Fathur Hidayat - Front End
- Fajri Fernanda - Back End
- Samuel Mulatua - Back End