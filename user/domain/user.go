package domain

import (
	"errors"
	"strings"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/samuelmjn/big-project/user/domain/helper"
)

type UserHelper interface {
	CheckIsUsernameExist(username string) bool
	CheckIsEmailExist(email string) bool
}

type User struct {
	UserID   string
	Username string
	Email    string
	Password string
	FullName string
	Token    string
}

func CreateUser(us UserHelper, username, email, password, fullname string) (User, error) {
	if len(password) <= 5 {
		return User{}, errors.New("Password minimum consists of 5 characters")
	} else if us.CheckIsUsernameExist(username) == true {
		return User{}, errors.New("Username is already used")
	} else if us.CheckIsEmailExist(email) == true {
		return User{}, errors.New("Email is already used")
	} else if username == "" {
		return User{}, errors.New("Username can't be null")
	} else if email == "" {
		return User{}, errors.New("Email can't be null")
	} else if password == "" {
		return User{}, errors.New("Password can't be null")
	} else if fullname == "" {
		return User{}, errors.New("Full Name can't be null")
	} else if strings.Contains(email, "@") == false {
		return User{}, errors.New("Invalid Email format")
	}
	return User{
		UserID:   uuid.NewV4().String(),
		Username: username,
		Email:    email,
		Password: password,
		FullName: fullname,
		Token:    helper.TokenGenerator(),
	}, nil
}
