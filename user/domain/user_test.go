package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type UserServiceExists struct {
}

func (us UserServiceExists) CheckIsUsernameExist(string) bool {
	return true
}

func (us UserServiceExists) CheckIsEmailExist(string) bool {
	return false
}

type UserServiceNotExists struct {
}

func (us UserServiceNotExists) CheckIsUsernameExist(string) bool {
	return false
}

func (us UserServiceNotExists) CheckIsEmailExist(string) bool {
	return false
}
func TestCanCreateArticle(t *testing.T) {
	//Given
	username := "anto"
	email := "anto@gmail.com"
	password := "anto123"
	fullname := "Anto Rahanto"
	userService := UserServiceNotExists{}
	//When
	user, err := CreateUser(userService, username, email, password, fullname)
	//Then
	assert.Nil(t, err)
	assert.Equal(t, user.Email, email)
}

func TestCantCreateArticle(t *testing.T) {
	//Given
	username := "anto"
	email := "anto@gmail.com"
	password := "anto123"
	fullname := "Anto Rahanto"
	userService := UserServiceExists{}
	//When
	user, err := CreateUser(userService, username, email, password, fullname)
	//Then
	assert.Equal(t, user.FullName, "")
	assert.Error(t, err, "Username is already used")
}
