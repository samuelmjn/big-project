package server

import (
	"gitlab.com/samuelmjn/big-project/user/domain"
	"gitlab.com/samuelmjn/big-project/user/proto"
)

func ParseUserToProto(user domain.User) proto.User {
	return proto.User{
		UserID:   user.UserID,
		Username: user.Username,
		Email:    user.Email,
		Password: user.Password,
		Fullname: user.FullName,
		Token:    user.Token,
	}

}
