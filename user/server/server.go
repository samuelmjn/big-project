package server

import (
	"context"

	"gitlab.com/samuelmjn/big-project/user/domain"
	"gitlab.com/samuelmjn/big-project/user/proto"
)

type UserServerService interface {
	UserLogin(email, username, password string) (domain.User, error)
	CreateUser(email, username, password, fullname string) (domain.User, error)
	FindUserByID(id string) (domain.User, error)
	FindUserByToken(token string) (domain.User, error)
	FindUserByUsername(username string) (domain.User, error)
}

type UserServer struct {
	UserService UserServerService
}

func NewUserServer(services UserServerService) proto.UserServiceServer {
	return &UserServer{
		UserService: services,
	}
}

func (s *UserServer) UserLogin(ctx context.Context, params *proto.UserInput) (*proto.User, error) {
	email := params.Email
	username := params.Username
	password := params.Password

	user, err := s.UserService.UserLogin(email, username, password)
	if err != nil {
		return &proto.User{}, err
	}

	userResult := ParseUserToProto(user)
	return &userResult, nil
}

func (s *UserServer) CreateUser(ctx context.Context, params *proto.CreateUserInput) (*proto.User, error) {
	email := params.GetEmail()
	username := params.GetUsername()
	password := params.GetPassword()
	fullname := params.GetFullname()

	user, err := s.UserService.CreateUser(username, email, password, fullname)
	if err != nil {
		return nil, err
	}

	// userResult := ParseUserToProto(user)
	return &proto.User{
		UserID:   user.UserID,
		Username: user.Username,
		Email:    user.Email,
		Password: user.Password,
		Fullname: user.FullName,
		Token:    user.Token,
	}, nil
}

func (s *UserServer) FindUserByID(ctx context.Context, params *proto.UserIDInput) (*proto.User, error) {
	id := params.GetUserID()

	user, err := s.UserService.FindUserByID(id)
	if err != nil {
		return &proto.User{}, err
	}

	result := ParseUserToProto(user)

	return &result, nil
}

func (s *UserServer) FindUserByToken(ctx context.Context, params *proto.UserTokenInput) (*proto.User, error) {
	token := params.GetToken()

	user, err := s.UserService.FindUserByToken(token)
	if err != nil {
		return &proto.User{}, err
	}

	result := ParseUserToProto(user)

	return &result, nil
}

func (s *UserServer) FindUserByUsername(ctx context.Context, params *proto.UsernameInput) (*proto.User, error) {
	username := params.GetUsername()

	user, err := s.UserService.FindUserByUsername(username)
	if err != nil {
		return &proto.User{}, err
	}

	result := ParseUserToProto(user)

	return &result, nil
}
