package service

import (
	"gitlab.com/samuelmjn/big-project/user/container"
	"gitlab.com/samuelmjn/big-project/user/domain"
	"gitlab.com/samuelmjn/big-project/user/query"
	"gitlab.com/samuelmjn/big-project/user/repository"
)

type UserService struct {
	Query      query.UserQuery
	Repository repository.UserRepository
}

func NewUserService(app *container.App) *UserService {

	return &UserService{
		Query:      app.User.Query,
		Repository: app.User.Repository,
	}
}

func (s *UserService) UserLogin(email, username, password string) (domain.User, error) {
	result := s.Query.UserLogin(email, username, password)
	if result.Error != nil {
		return domain.User{}, result.Error
	}
	err := s.Repository.SaveToken(result.Result.(domain.User).UserID, result.Result.(domain.User).Token)
	if err != nil {
		return result.Result.(domain.User), err
	}
	return result.Result.(domain.User), nil
}

func (s *UserService) CreateUser(username, email, password, fullname string) (domain.User, error) {
	user, err := domain.CreateUser(s, username, email, password, fullname)
	if err != nil {
		return domain.User{}, err
	}
	err = s.Repository.SaveUser(&user)
	if err != nil {
		return domain.User{}, err
	}
	return user, nil
}

func (s *UserService) CheckIsUsernameExist(username string) bool {
	exists := s.Query.CheckIsUsernameExist(username)
	if exists {
		return true
	} else {
		return false
	}
}

func (s *UserService) CheckIsEmailExist(email string) bool {
	exists := s.Query.CheckIsEmailExist(email)
	if exists {
		return true
	} else {
		return false
	}
}

func (s *UserService) FindUserByID(id string) (domain.User, error) {
	user := s.Query.FindUserByID(id)
	if user.Error != nil {
		return domain.User{}, user.Error
	} else {
		return user.Result.(domain.User), nil
	}

}

func (s *UserService) FindUserByToken(token string) (domain.User, error) {
	user := s.Query.FindUserByToken(token)
	if user.Error != nil {
		return domain.User{}, user.Error
	} else {
		return user.Result.(domain.User), nil
	}

}

func (s *UserService) FindUserByUsername(username string) (domain.User, error) {
	user := s.Query.FindUserByUsername(username)
	if user.Error != nil {
		return domain.User{}, user.Error
	} else {
		return user.Result.(domain.User), nil
	}
}
