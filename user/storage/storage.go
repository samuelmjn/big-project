package storage

import "gitlab.com/samuelmjn/big-project/user/domain"

type UserStorage struct {
	UserMap []domain.User
}

func NewUserStorage() *UserStorage {
	sampleUser := &domain.User{
		UserID:   "9237f27-aed7-4390-b3bd-80aadcb974ba",
		Username: "bambang",
		Email:    "bambang@gmail.com",
		Password: "bambang123",
		FullName: "Bambang Setyanto",
		Token:    "",
	}
	sampleUser2 := &domain.User{
		UserID:   "92237f27-aed7-4390-b3bd-80aadcb974ba",
		Username: "jri",
		Email:    "jri@gmail.com",
		Password: "jri",
		FullName: "jri iii",
		Token:    "",
	}
	return &UserStorage{
		UserMap: []domain.User{*sampleUser, *sampleUser2},
	}
}
