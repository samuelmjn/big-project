package inmemory

import (
	"errors"

	"gitlab.com/samuelmjn/big-project/user/domain"
	"gitlab.com/samuelmjn/big-project/user/domain/helper"
	"gitlab.com/samuelmjn/big-project/user/query"
	"gitlab.com/samuelmjn/big-project/user/storage"
)

type UserQueryInMemory struct {
	Storage *storage.UserStorage
}

func NewUserQueryInMemory(storage *storage.UserStorage) query.UserQuery {
	return &UserQueryInMemory{Storage: storage}
}

func (q *UserQueryInMemory) UserLogin(email, username, password string) query.QueryResult {
	generatedToken := helper.TokenGenerator()
	userResult := domain.User{}
	for _, user := range q.Storage.UserMap {
		if email == "" {
			if user.Username == username && user.Password == password {
				userResult = user
			}
		} else if username == "" {
			if user.Email == email && user.Password == password {
				userResult = user
			}
		}
	}
	if userResult.Username == "" {
		return query.QueryResult{Result: domain.User{}, Error: errors.New("Invalid Username or Password")}
	} else {

		return query.QueryResult{Result: domain.User{
			UserID:   userResult.UserID,
			Username: userResult.Username,
			Email:    userResult.Email,
			FullName: userResult.FullName,
			Token:    generatedToken,
		}, Error: nil}
	}
}

func (q *UserQueryInMemory) CheckIsUsernameExist(username string) bool {
	for _, user := range q.Storage.UserMap {
		if user.Username == username {
			return true
		}
	}
	return false
}

func (q *UserQueryInMemory) CheckIsEmailExist(email string) bool {
	for _, user := range q.Storage.UserMap {
		if user.Email == email {
			return true
		}
	}
	return false
}

func (q *UserQueryInMemory) FindUserByID(id string) query.QueryResult {
	userResult := domain.User{}
	for _, user := range q.Storage.UserMap {
		if user.UserID == id {
			userResult = user
		}
	}
	if userResult.Email == "" {
		return query.QueryResult{Result: userResult, Error: errors.New("User not found")}
	}
	return query.QueryResult{Result: userResult, Error: nil}
}

func (q *UserQueryInMemory) FindUserByToken(token string) query.QueryResult {
	userResult := domain.User{}
	for _, user := range q.Storage.UserMap {
		if user.Token == token {
			userResult = user
		}
	}
	if userResult.Token == "" {
		return query.QueryResult{Result: userResult, Error: errors.New("Token not found")}
	}
	return query.QueryResult{Result: userResult, Error: nil}
}

func (q *UserQueryInMemory) FindUserByUsername(username string) query.QueryResult {
	userResult := domain.User{}
	for _, user := range q.Storage.UserMap {
		if user.Username == username {
			userResult = user
		}
	}
	if userResult.Username == "" {
		return query.QueryResult{Result: userResult, Error: errors.New("Username not found")}
	}
	return query.QueryResult{Result: userResult, Error: nil}
}
