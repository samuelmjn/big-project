package inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/samuelmjn/big-project/user/domain"
	"gitlab.com/samuelmjn/big-project/user/storage"
)

func TestUserCanLoginWithEmail(t *testing.T) {
	// Given
	email := "bambang@gmail.com"
	username := ""
	password := "bambang123"
	// When
	db := storage.NewUserStorage()
	query := NewUserQueryInMemory(db)
	result := query.UserLogin(email, username, password)
	// Then
	assert.Equal(t, result.Result.(domain.User).Email, email)
	assert.Nil(t, result.Error)
}

func TestUserCanLoginWithUserName(t *testing.T) {
	// Given
	email := ""
	username := "bambang"
	password := "bambang123"
	// When
	db := storage.NewUserStorage()
	query := NewUserQueryInMemory(db)
	result := query.UserLogin(email, username, password)
	// Then
	assert.Equal(t, result.Result.(domain.User).Username, username)
	assert.Equal(t, result.Result.(domain.User).Password, "")
	assert.Nil(t, result.Error)
}

func TestUserCantLoginWithWrongCredentials(t *testing.T) {
	// Given
	email := ""
	username := "blahblah"
	password := "bambang123"
	// When
	db := storage.NewUserStorage()
	query := NewUserQueryInMemory(db)
	result := query.UserLogin(email, username, password)
	// Then
	assert.Error(t, result.Error)
}
