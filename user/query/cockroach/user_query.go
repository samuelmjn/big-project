package cockroach

import (
	"database/sql"
	"errors"

	"gitlab.com/samuelmjn/big-project/user/domain"
	"gitlab.com/samuelmjn/big-project/user/domain/helper"
	"gitlab.com/samuelmjn/big-project/user/query"
)

type UserQueryCockroach struct {
	DB *sql.DB
}

func NewUserQueryCockroach(DB *sql.DB) query.UserQuery {
	return &UserQueryCockroach{DB: DB}
}

func (q *UserQueryCockroach) UserLogin(email, username, password string) query.QueryResult {
	generatedToken := helper.TokenGenerator()
	user := domain.User{}
	err := errors.New("")
	if username == "" {
		err = q.DB.QueryRow(`SELECT * FROM "USER" WHERE "EMAIL" = $1 AND "PASSWORD" = $2`, email, password).Scan(&user.UserID, &user.Username, &user.Email, &user.Password, &user.FullName, &user.Token)
	} else if email == "" {
		err = q.DB.QueryRow(`SELECT * FROM "USER" WHERE "USERNAME" = $1 AND "PASSWORD" = $2`, username, password).Scan(&user.UserID, &user.Username, &user.Email, &user.Password, &user.FullName, &user.Token)
	}
	if err != nil {
		return query.QueryResult{Result: user, Error: errors.New("User not found")}
	}
	user.Token = generatedToken
	return query.QueryResult{Result: user, Error: nil}
}

func (q *UserQueryCockroach) CheckIsUsernameExist(username string) bool {
	var count int
	err := q.DB.QueryRow(`SELECT COUNT (*) FROM "USER" WHERE "USERNAME" = $1`, username).Scan(&count)
	if err != nil {
		return true
	}
	if count != 0 {
		return true
	}

	return false
}

func (q *UserQueryCockroach) CheckIsEmailExist(email string) bool {
	var count int
	err := q.DB.QueryRow(`SELECT COUNT (*) FROM "USER" WHERE "EMAIL" = $1`, email).Scan(&count)
	if err != nil {
		return true
	}
	if count != 0 {
		return true
	}

	return false
}

func (q *UserQueryCockroach) FindUserByID(id string) query.QueryResult {
	user := domain.User{}
	err := q.DB.QueryRow(`SELECT * FROM "USER" WHERE "USERID" = $1`, id).Scan(&user.UserID, &user.Username, &user.Email, &user.Password, &user.FullName, &user.Token)
	if err != nil {
		return query.QueryResult{Result: user, Error: err}
	}
	return query.QueryResult{Result: user, Error: nil}
}

func (q *UserQueryCockroach) FindUserByToken(token string) query.QueryResult {
	user := domain.User{}
	err := q.DB.QueryRow(`SELECT * FROM "USER" WHERE "TOKEN" = $1`, token).Scan(&user.UserID, &user.Username, &user.Email, &user.Password, &user.FullName, &user.Token)
	if err != nil {
		return query.QueryResult{Result: user, Error: err}
	}
	return query.QueryResult{Result: user, Error: nil}
}

func (q *UserQueryCockroach) FindUserByUsername(username string) query.QueryResult {
	user := domain.User{}
	err := q.DB.QueryRow(`SELECT * FROM "USER" WHERE "USERNAME" = $1`, username).Scan(&user.UserID, &user.Username, &user.Email, &user.Password, &user.FullName, &user.Token)
	if err != nil {
		return query.QueryResult{Result: user, Error: err}
	}
	return query.QueryResult{Result: user, Error: nil}
}
