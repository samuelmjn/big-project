package query

type QueryResult struct {
	Result interface{}
	Error  error
}

type UserQuery interface {
	UserLogin(email, username, password string) QueryResult
	CheckIsUsernameExist(username string) bool
	CheckIsEmailExist(email string) bool
	FindUserByID(id string) QueryResult
	FindUserByToken(token string) QueryResult
	FindUserByUsername(username string) QueryResult
}
