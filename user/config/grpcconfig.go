package config

import (
	"os"

	"github.com/joho/godotenv"
)

var (
	// ENV store environment variable
	ENV string

	// GRPCArticleServerPort port for serving article service
	UserServiceGRPCServerPort = "user:9100"
)

//InitPort to load env for port
func InitPort() {
	godotenv.Load()

	// Check the environment variable
	env := os.Getenv("ENV")
	if env != "production" {
		ENV = "development"
		UserServiceGRPCServerPort = ":9100"

	} else {
		UserServiceGRPCServerPort = "user:9100"
		ENV = "production"
	}
}
