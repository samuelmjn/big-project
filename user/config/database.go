package config

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

func InitCockroach() *sql.DB {
	// dsn := "postgresql://root@database:26257/sslmode=disable"
	// db, err := sql.Open("postgres", dsn)
	// db.Exec(`CREATE DATABASE IF NOT EXISTS userdb`)
	// db.Close()

	dsn := "postgresql://root@localhost:26257/userdb?sslmode=disable"
	dbn, err := sql.Open("postgres", dsn)
	log.Println(err)

	// ddl, err := ioutil.ReadFile("config/ddl.sql")
	// if err != nil {
	// 	fmt.Println(err)
	// 	panic("Error, cannot run ddl")
	// }
	// sql := string(ddl)
	// _, err = dbn.Exec(sql)
	// if err != nil {
	// 	panic(err)
	// }
	return dbn
}
