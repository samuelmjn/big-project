package container

import (
	"gitlab.com/samuelmjn/big-project/user/config"
	"gitlab.com/samuelmjn/big-project/user/query"
	cq "gitlab.com/samuelmjn/big-project/user/query/cockroach"
	iq "gitlab.com/samuelmjn/big-project/user/query/inmemory"
	"gitlab.com/samuelmjn/big-project/user/repository"
	cr "gitlab.com/samuelmjn/big-project/user/repository/cockroach"
	ir "gitlab.com/samuelmjn/big-project/user/repository/inmemory"
	"gitlab.com/samuelmjn/big-project/user/storage"
)

type User struct {
	Query      query.UserQuery
	Repository repository.UserRepository
}

type App struct {
	User User
}

func InitApp() *App {
	// driver := ""
	driver := "cockroach"
	switch driver {
	case "cockroach":
		db := config.InitCockroach()

		return &App{
			User: User{
				Query:      cq.NewUserQueryCockroach(db),
				Repository: cr.NewUserRepositoryCockroach(db),
			},
		}
	default:
		db := storage.NewUserStorage()
		return &App{
			User: User{
				Query:      iq.NewUserQueryInMemory(db),
				Repository: ir.NewUserRepositoryInMemory(db),
			},
		}
	}

}
