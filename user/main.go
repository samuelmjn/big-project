package main

import (
	"log"
	"net"

	"gitlab.com/samuelmjn/big-project/user/config"
	"gitlab.com/samuelmjn/big-project/user/container"

	"gitlab.com/samuelmjn/big-project/user/proto"
	"gitlab.com/samuelmjn/big-project/user/server"
	"gitlab.com/samuelmjn/big-project/user/service"
	"google.golang.org/grpc"
)

func main() {
	config.InitPort()
	srv := grpc.NewServer()
	app := container.InitApp()
	service := service.NewUserService(app)
	userServer := server.NewUserServer(service)
	proto.RegisterUserServiceServer(srv, userServer)
	log.Println("Starting gRPC Server at", config.UserServiceGRPCServerPort)
	listen, err := net.Listen("tcp", config.UserServiceGRPCServerPort)
	if err != nil {
		log.Fatalf("could not listen to %s: %v", config.UserServiceGRPCServerPort, err)
	}

	log.Fatal("Server is listening", srv.Serve(listen))
}
