package cockroach

import (
	"database/sql"

	"gitlab.com/samuelmjn/big-project/user/domain"
	"gitlab.com/samuelmjn/big-project/user/repository"
)

type UserRepositoryCockroach struct {
	DB *sql.DB
}

func NewUserRepositoryCockroach(DB *sql.DB) repository.UserRepository {
	return &UserRepositoryCockroach{DB: DB}
}

func (r *UserRepositoryCockroach) SaveUser(user *domain.User) error {
	_, err := r.DB.Exec(`INSERT INTO "USER" VALUES ($1,$2,$3,$4,$5,$6)`,
		user.UserID,
		user.Username,
		user.Email,
		user.Password,
		user.FullName,
		user.Token)

	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepositoryCockroach) SaveToken(userid, token string) error {
	_, err := r.DB.Exec(`UPDATE "USER" SET "TOKEN" = $1 WHERE "USERID" = $2`, token, userid)
	if err != nil {
		return err
	}
	return nil
}
