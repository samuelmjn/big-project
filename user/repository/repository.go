package repository

import "gitlab.com/samuelmjn/big-project/user/domain"

type UserRepository interface {
	SaveUser(user *domain.User) error
	SaveToken(userid, token string) error
}
