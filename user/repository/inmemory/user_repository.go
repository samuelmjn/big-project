package inmemory

import (
	"gitlab.com/samuelmjn/big-project/user/domain"
	"gitlab.com/samuelmjn/big-project/user/repository"
	"gitlab.com/samuelmjn/big-project/user/storage"
)

type UserRepositoryInMemory struct {
	Storage *storage.UserStorage
}

func NewUserRepositoryInMemory(storage *storage.UserStorage) repository.UserRepository {
	return &UserRepositoryInMemory{Storage: storage}
}

func (repo *UserRepositoryInMemory) SaveUser(user *domain.User) error {
	repo.Storage.UserMap = append(repo.Storage.UserMap, *user)
	return nil
}

func (repo *UserRepositoryInMemory) SaveToken(userid, token string) error {
	for k, v := range repo.Storage.UserMap {
		if v.UserID == userid {
			repo.Storage.UserMap[k].Token = token
		}
	}

	return nil
}
