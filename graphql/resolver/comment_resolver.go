package resolver

import (
	"context"

	"gitlab.com/samuelmjn/big-project/graphql/structure"
)

type CommentResolver struct {
	meta structure.Comment
}

func (r *CommentResolver) Message(ctx context.Context) *string {
	return &r.meta.Message
}

func (r *CommentResolver) Fullname(ctx context.Context) *string {
	return &r.meta.Fullname
}

func (r *CommentResolver) Username(ctx context.Context) *string {
	return &r.meta.Username
}

func (r *CommentResolver) ArticleID(ctx context.Context) *string {
	return &r.meta.ArticleID
}
