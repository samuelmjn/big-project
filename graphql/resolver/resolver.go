package resolver

import (
	"gitlab.com/samuelmjn/big-project/graphql/client"
)

var UserService client.UserClient
var CommentService client.CommentClient
var ArticleService client.ArticleClient

type Resolver struct {
	ArticleService client.ArticleClient
	UserService    client.UserClient
	CommentService client.CommentClient
}

func init() {
	UserService = client.NewUserGRPCClient()
	CommentService = client.NewCommentGRPCClient()
	ArticleService = client.NewArticleGRPCClient()
}
