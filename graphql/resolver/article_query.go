package resolver

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/samuelmjn/big-project/graphql/structure"
)

func (r *Resolver) GetPublishedArticlesByCategory(ctx context.Context, args struct {
	Category string
	Offset   int32
	Limit    int32
}) (*[]*ArticleResolver, error) {
	articleListResponse, err := r.ArticleService.GetPublishedArticlesByCategory(args.Category, args.Offset, args.Limit)
	articles := articleListResponse
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))
	for i, v := range articles {
		result[i] = &ArticleResolver{
			meta: structure.Article{
				Id:            v.Id,
				Slug:          v.Slug,
				Body:          v.Body,
				Title:         v.Title,
				CreatedAt:     v.CreatedAt,
				UpdatedAt:     v.UpdatedAt,
				UserID:        v.UserID,
				Status:        v.Status,
				Thumbnail:     v.Thumbnail,
				Categories:    v.Categories,
				User:          v.User,
				Totalcomments: v.Totalcomments,
			},
		}
	}
	return &result, nil
}

func (r *Resolver) GetPublishedArticleBySlug(ctx context.Context, args struct{ Slug string }) (*ArticleResolver, error) {
	article, err := r.ArticleService.GetArticleBySlug(args.Slug)
	if err != nil {
		return nil, err
	}

	result := &ArticleResolver{
		meta: structure.Article{
			Id:            article.Id,
			Slug:          article.Slug,
			Body:          article.Body,
			Title:         article.Title,
			CreatedAt:     article.CreatedAt,
			UpdatedAt:     article.UpdatedAt,
			UserID:        article.UserID,
			Status:        article.Status,
			Thumbnail:     article.Thumbnail,
			Categories:    article.Categories,
			User:          article.User,
			Totalcomments: article.Totalcomments,
		},
	}

	return result, nil
}

func (r *Resolver) FindPublishedArticles(ctx context.Context, args struct {
	Keyword string
	Offset  int32
	Limit   int32
}) (*[]*ArticleResolver, error) {

	articleListResponse, err := r.ArticleService.FindPublishedArticles(args.Keyword, args.Offset, args.Limit)
	articles := articleListResponse
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))
	for i, v := range articles {
		result[i] = &ArticleResolver{
			meta: structure.Article{
				Id:            v.Id,
				Slug:          v.Slug,
				Body:          v.Body,
				Title:         v.Title,
				CreatedAt:     v.CreatedAt,
				UpdatedAt:     v.UpdatedAt,
				UserID:        v.UserID,
				Status:        v.Status,
				Thumbnail:     v.Thumbnail,
				Categories:    v.Categories,
				User:          v.User,
				Totalcomments: v.Totalcomments,
			},
		}
	}
	return &result, nil
}

func (r *Resolver) GetUserCategoriesByUsername(ctx context.Context, args struct {
	Username string
	Offset   int32
	Limit    int32
}) (*[]*CategoryResolver, error) {
	user, _ := r.UserService.FindUserByUsername(args.Username)

	categories, err := r.ArticleService.GetUserCategoriesByUserid(user.UserID, args.Offset, args.Limit)
	result := make([]*CategoryResolver, len(categories))
	if err != nil {
		return &result, err
	}
	for i, v := range categories {
		result[i] = &CategoryResolver{
			meta: structure.Category{
				UserID:       v.UserID,
				Categoryname: v.Categoryname,
				Categoryslug: v.Categoryslug,
			},
		}
	}

	return &result, nil
}

func (r *Resolver) ProfileArticles(ctx context.Context, args struct {
	Username string
	Category string
	Offset   int32
	Limit    int32
}) (*ProfileResolver, error) {
	resp := ProfileResolver{}

	user, err := r.UserService.FindUserByUsername(args.Username)
	if err != nil {
		return &resp, nil
	}
	resp.user = UserResolver{
		meta: user,
	}

	articles, err := r.ArticleService.ProfileArticles(user.UserID, args.Category, args.Offset, args.Limit)
	var resultArticles []*ArticleResolver
	if err != nil {
		return &resp, nil
	}
	for _, v := range articles {
		resultArticles = append(resultArticles, &ArticleResolver{
			meta: structure.Article{
				Id:         v.Id,
				Slug:       v.Slug,
				Body:       v.Body,
				Title:      v.Title,
				CreatedAt:  v.CreatedAt,
				UpdatedAt:  v.UpdatedAt,
				Status:     v.Status,
				UserID:     v.UserID,
				Thumbnail:  v.Thumbnail,
				Categories: v.Categories,
			},
		})

	}

	resp.articles = resultArticles

	return &resp, nil

}

func (r *Resolver) DashboardArticles(ctx context.Context, args struct {
	Keyword  string
	Category string
	Status   string
	Offset   int32
	Limit    int32
}) (*[]*ArticleResolver, error) {
	userId := ""
	if ctx.Value("user") != nil {
		user := ctx.Value("user").(string)
		currentUser := strings.Split(user, ";")[0] //username
		userId = strings.Split(user, ";")[1]
		fmt.Println("----", userId)
		if currentUser == "" {
			return &[]*ArticleResolver{}, errors.New("couldn't authorize user")
		}
	} else {
		return &[]*ArticleResolver{}, errors.New("couldn't authorize user")
	}

	articleListResponse, err := r.ArticleService.DashboardArticles(userId, args.Keyword, args.Category, args.Status, args.Offset, args.Limit)
	articles := articleListResponse
	fmt.Println("---------", articles)
	if err != nil {
		return nil, err
	}

	result := make([]*ArticleResolver, len(articles))
	for i, v := range articles {
		result[i] = &ArticleResolver{
			meta: structure.Article{
				Id:            v.Id,
				Slug:          v.Slug,
				Body:          v.Body,
				Title:         v.Title,
				CreatedAt:     v.CreatedAt,
				UpdatedAt:     v.UpdatedAt,
				UserID:        v.UserID,
				Status:        v.Status,
				Thumbnail:     v.Thumbnail,
				Categories:    v.Categories,
				User:          v.User,
				Totalcomments: v.Totalcomments,
			},
		}
	}
	return &result, nil
}

func (r *Resolver) GetCommentsByArticleID(ctx context.Context, args struct {
	ArticleId string
	Offset    int32
	Limit     int32
}) (*[]*CommentResolver, error) {
	commentListResponse, err := r.CommentService.GetComments(args.ArticleId, args.Limit, args.Offset)
	comments := commentListResponse
	if err != nil {
		return nil, err
	}

	result := make([]*CommentResolver, len(comments))
	for i, v := range comments {
		result[i] = &CommentResolver{
			meta: structure.Comment{
				CommentID: v.CommentID,
				ArticleID: v.ArticleID,
				Message:   v.Message,
				Username:  v.Username,
				Fullname:  v.Fullname,
			},
		}
	}
	return &result, nil
}
