package resolver

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

type ArticleInput struct {
	Id         *string
	Title      *string
	Slug       *string
	Body       *string
	Status     *string
	Categories *[]*string
	Thumbnail  *string
	Username   *string
}

// CreateArticle to create
func (r *Resolver) CreateArticle(ctx context.Context, args struct{ Article ArticleInput }) (*ArticleResolver, error) {
	if ctx.Value("user") != nil {
		user := ctx.Value("user").(string)
		userCtx := strings.Split(user, ";")
		if userCtx[0] != *args.Article.Username {
			return &ArticleResolver{}, errors.New("couldn't authorize user")
		}
	} else {
		return &ArticleResolver{}, errors.New("couldn't authorize user")
	}
	Body := *args.Article.Body
	Title := *args.Article.Title
	Slug := *args.Article.Slug
	user := ctx.Value("user").(string)
	UserId := strings.Split(user, ";")[1]
	Status := *args.Article.Status
	Thumbnail := *args.Article.Thumbnail
	var Categories []string
	for _, v := range *args.Article.Categories {
		Categories = append(Categories, *v)
	}
	result, err := r.ArticleService.CreateArticle(Title, Slug, Body, Thumbnail, UserId, Status, Categories)
	if err != nil {
		return nil, err
	}
	resolver := ArticleResolver{
		meta: result}

	return &resolver, nil
}

type CommentInput struct {
	Message   *string
	Username  *string
	Fullname  *string
	ArticleID *string
}

func (r *Resolver) CreateComment(ctx context.Context, args struct{ Comment CommentInput }) (*CommentResolver, error) {
	if ctx.Value("user") != nil {
		user := ctx.Value("user").(string)
		userCtx := strings.Split(user, ";")
		if userCtx[0] != *args.Comment.Username {
			return &CommentResolver{}, errors.New("couldn't authorize user")
		}
	} else {
		return &CommentResolver{}, errors.New("couldn't authorize user")
	}
	Message := *args.Comment.Message
	Username := *args.Comment.Username
	Fullname := *args.Comment.Fullname
	ArticleID := *args.Comment.ArticleID
	result, err := r.CommentService.CreateComment(Message, Username, Fullname, ArticleID)
	if err != nil {
		return nil, err
	}

	fmt.Println(result)
	resolver := CommentResolver{
		meta: result}

	return &resolver, nil
}

type CategoryInput struct {
	Username     *string
	Categoryname *string
	Categoryslug *string
}

func (r *Resolver) CreateUserCategory(ctx context.Context, args struct{ Category CategoryInput }) (*CategoryResolver, error) {
	userid := ""
	if ctx.Value("user") != nil {
		user := ctx.Value("user").(string)
		userCtx := strings.Split(user, ";")
		userid = userCtx[1]
		if userCtx[0] != *args.Category.Username {
			return &CategoryResolver{}, errors.New("couldn't authorize user")
		}
	} else {
		return &CategoryResolver{}, errors.New("couldn't authorize user")
	}
	UserID := userid
	Categoryname := *args.Category.Categoryname
	Categoryslug := *args.Category.Categoryslug
	_ = *args.Category.Categoryslug
	result, err := r.ArticleService.CreateUserCategory(UserID, Categoryname, Categoryslug)
	if err != nil {
		return nil, err
	}

	fmt.Println(result)
	resolver := CategoryResolver{
		meta: result}

	return &resolver, nil
}

func (r *Resolver) UpdateArticle(ctx context.Context, args struct{ Article ArticleInput }) (*ArticleResolver, error) {
	if ctx.Value("user") != nil {
		user := ctx.Value("user").(string)
		userCtx := strings.Split(user, ";")
		if userCtx[0] != *args.Article.Username {
			return &ArticleResolver{}, errors.New("couldn't authorize user")
		}
	} else {
		return &ArticleResolver{}, errors.New("couldn't authorize user")
	}
	Body := *args.Article.Body
	Title := *args.Article.Title
	Slug := *args.Article.Slug
	user := ctx.Value("user").(string)
	UserId := strings.Split(user, ";")[1]
	Id := *args.Article.Id
	Status := *args.Article.Status
	Thumbnail := *args.Article.Thumbnail
	var Categories []string
	for _, v := range *args.Article.Categories {
		Categories = append(Categories, *v)
	}
	result, err := r.ArticleService.UpdateArticle(Id, UserId, Title, Slug, Body, Thumbnail, Status, Categories)

	if err != nil {
		return &ArticleResolver{}, err
	}

	fmt.Println("result", result)
	resolver := ArticleResolver{
		meta: result}

	return &resolver, nil
}
