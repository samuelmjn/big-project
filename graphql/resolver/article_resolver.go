package resolver

import (
	"context"
	"log"

	"gitlab.com/samuelmjn/big-project/graphql/structure"
)

type ArticleResolver struct {
	meta structure.Article
}

// ID to resolve id
func (r *ArticleResolver) Id(ctx context.Context) *string {
	return &r.meta.Id
}

// Name is to resolve
func (r *ArticleResolver) Title(ctx context.Context) *string {
	return &r.meta.Title
}

// Slug is to resolve slug name
func (r *ArticleResolver) Slug(ctx context.Context) *string {
	return &r.meta.Slug
}

// Body is to resolve body
func (r *ArticleResolver) Body(ctx context.Context) *string {
	return &r.meta.Body
}

func (r *ArticleResolver) Status(ctx context.Context) *string {
	return &r.meta.Status
}

func (r *ArticleResolver) Categories(ctx context.Context) *[]*CategoryResolver {
	var categories []*CategoryResolver
	for _, v := range r.meta.Categories {
		catResolver := &CategoryResolver{
			meta: v,
		}
		categories = append(categories, catResolver)
	}
	return &categories
}

func (r *ArticleResolver) Thumbnail(ctx context.Context) *string {
	return &r.meta.Thumbnail
}

func (r *ArticleResolver) UserId(ctx context.Context) *string {
	return &r.meta.UserID
}

// CreatedAt is to resolve created_at
func (r *ArticleResolver) CreatedAt(ctx context.Context) *string {
	ca := r.meta.CreatedAt.String()
	return &ca
}

// UpdatedAt is to resolve updated_at
func (r *ArticleResolver) UpdatedAt(ctx context.Context) *string {
	ua := r.meta.UpdatedAt.String()
	return &ua
}

func (r *ArticleResolver) User(ctx context.Context) *UserResolver {
	user, err := UserService.FindUserByID(r.meta.UserID)
	log.Println(err)
	userResolver := UserResolver{
		meta: user,
	}

	return &userResolver
}

func (r *ArticleResolver) TotalComments(ctx context.Context) *int32 {
	totalcomments := CommentService.GetCommentsCount(r.meta.Id)
	return &totalcomments
}

type UserResolver struct {
	meta structure.User
}

func (r *UserResolver) Fullname(ctx context.Context) *string {
	return &r.meta.FullName
}

func (r *UserResolver) Username(ctx context.Context) *string {
	return &r.meta.Username
}
func (r *UserResolver) UserId(ctx context.Context) *string {
	return &r.meta.UserID
}
func (r *UserResolver) Email(ctx context.Context) *string {
	return &r.meta.Email
}
func (r *UserResolver) Token(ctx context.Context) *string {
	return &r.meta.Token
}

type CategoryResolver struct {
	meta structure.Category
}

func (r *CategoryResolver) UserId(ctx context.Context) *string {
	return &r.meta.UserID
}
func (r *CategoryResolver) Categoryname(ctx context.Context) *string {
	return &r.meta.Categoryname
}
func (r *CategoryResolver) Categoryslug(ctx context.Context) *string {
	return &r.meta.Categoryslug
}
func (r *CategoryResolver) CategoryId(ctx context.Context) *string {
	return &r.meta.CategoryID
}
