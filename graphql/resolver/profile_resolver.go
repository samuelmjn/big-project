package resolver

import (
	"context"
)

type ProfileResolver struct {
	user     UserResolver
	articles []*ArticleResolver
}

func (r *ProfileResolver) User(ctx context.Context) *UserResolver {
	return &r.user

}

func (r *ProfileResolver) Articles(ctx context.Context) *[]*ArticleResolver {
	return &r.articles
}
