package cloudinary

import (
	"flag"
	"log"

	// "os"
	"sync"
)

type singleton struct {
}

var instance *Service
var once sync.Once

var accountKey = "149332267162931"
var secretKey = "JMgIbaVuwuA1KDBT1dkYE_kwQoc"
var cloudName = "duv3dli2f"

func GetService() *Service {
	flag.Parse()
	endpoint := "cloudinary://" + accountKey + ":" + secretKey + "@" + cloudName
	once.Do(func() {
		var err error
		instance, err = Dial(endpoint)
		if err != nil {
			log.Fatal(err)
		}
	})
	return instance
}
