package main

import (
	"context"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/samuelmjn/big-project/graphql/cloudinary"
	"gitlab.com/samuelmjn/big-project/graphql/config"

	graphql "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/samuelmjn/big-project/graphql/client"
	m "gitlab.com/samuelmjn/big-project/graphql/middleware"
	"gitlab.com/samuelmjn/big-project/graphql/resolver"
	"gitlab.com/samuelmjn/big-project/graphql/structure"
)

// Get schema helper
func getSchema(path string) (string, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return "", err
	}
	return string(b), nil
}

func main() {

	//Load Schema
	s, err := getSchema("./schema/schema.graphql")
	if err != nil {
		panic(err)
	}
	config.InitPort()
	resolver := &resolver.Resolver{ArticleService: client.NewArticleGRPCClient(), UserService: client.NewUserGRPCClient(), CommentService: client.NewCommentGRPCClient()}
	schema := graphql.MustParseSchema(s, resolver)

	e := echo.New()

	e.Use(middleware.Recover())
	e.Use(m.LogrusMiddleware())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderAuthorization, echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderXRequestedWith},
	}))
	e.Use(middleware.Static("/image"))
	e.Static("/image", "image")
	e.POST("/user/login", LoginHandler)
	e.POST("/user/register", CreateUserHandler)
	e.POST("/upload", UploadCloudinary)
	e.POST("/graphql", wrapHandler(&relay.Handler{Schema: schema}))

	e.Logger.Fatal(e.Start(":9000"))
}

func LoginHandler(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")
	email := c.FormValue("email")
	grpcConn := client.NewUserGRPCClient()
	user, err := grpcConn.UserLogin(username, email, password)
	if err != nil {
		return c.JSON(http.StatusUnauthorized, map[string]error{
			"error": err,
		})
	}

	return c.JSON(http.StatusOK, map[string]string{
		"username": user.Username,
		"email":    user.Email,
		"fullname": user.FullName,
		"token":    user.Token,
	})
}

func CreateUserHandler(c echo.Context) error {
	username := c.FormValue("username")
	password := c.FormValue("password")
	email := c.FormValue("email")
	fullname := c.FormValue("fullname")
	grpcConn := client.NewUserGRPCClient()
	user, err := grpcConn.CreateUser(email, username, password, fullname)
	if err != nil {
		return c.JSON(http.StatusUnauthorized, map[string]error{
			"error": err,
		})
	}
	return c.JSON(http.StatusOK, map[string]string{
		"username": user.Username,
		"email":    user.Email,
		"fullname": user.FullName,
		"token":    user.Token,
	})
}

func wrapHandler(h http.Handler) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Original Request
		originalRequest := c.Request()
		originalContext := originalRequest.Context()
		user := structure.User{}
		err := errors.New("")
		req := originalRequest
		authHeader := originalRequest.Header.Get("Authorization")
		if strings.Contains(authHeader, "Bearer") {
			tokenString := strings.Replace(authHeader, "Bearer ", "", -1)
			log.Println(tokenString)
			authClient := client.NewUserGRPCClient()
			user, err = authClient.FindUserByToken(tokenString)
			if err != nil {
				log.Println(err)
				return c.String(http.StatusUnauthorized, "you've using a wrong token")
			}

			contextUser := user.Username + ";" + user.UserID
			req = originalRequest.WithContext(
				context.WithValue(originalContext, "user", contextUser),
			)

		}
		log.Println("request", req)
		h.ServeHTTP(c.Response(), req)
		return nil
	}
}

func Upload(c echo.Context) error {
	// Source
	file, err := c.FormFile("file")
	if err != nil {
		return err
	}
	filenamestring := strings.Split(file.Filename, ".")
	fileExt := filenamestring[len(filenamestring)-1]
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	// Destination
	if err != nil {
		log.Fatal(err)
	}

	newFileName := uuid.NewV4().String() + "." + fileExt
	dst, err := os.Create("image/" + newFileName)
	if err != nil {
		return err
	}
	defer dst.Close()

	// Copy
	if _, err = io.Copy(dst, src); err != nil {
		return err
	}
	imageUrl := "/image/" + newFileName
	response := map[string]string{
		"imageUrl": imageUrl,
	}
	return c.JSON(http.StatusOK, response)
}

func UploadCloudinary(c echo.Context) error {
	file, err := c.FormFile("image")
	if err != nil {
		return err
	}
	src, err := file.Open()
	if err != nil {
		return err
	}

	defer src.Close()

	// Destination
	dst, err := os.Create(file.Filename)
	if err != nil {
		return err
	}
	defer dst.Close()

	// Copy
	if _, err = io.Copy(dst, src); err != nil {
		return err
	}

	service := cloudinary.GetService()
	url, err := service.UploadFile("imageblob", dst.Name(), nil)
	if err != nil {
		return err
	}
	os.Remove(dst.Name())
	return c.JSON(http.StatusOK, struct {
		URL string `json:"url"`
	}{URL: url})

}
