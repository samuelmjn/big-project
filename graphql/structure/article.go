package structure

import "time"

type Article struct {
	Id            string
	Title         string
	Slug          string
	Body          string
	Status        string
	CreatedAt     time.Time
	UpdatedAt     time.Time
	Categories    []Category
	Thumbnail     string
	UserID        string
	Totalcomments int32
	Comments      []Comment
	User          User
}

type Category struct {
	UserID       string
	Categoryname string
	Categoryslug string
	CategoryID   string
}
