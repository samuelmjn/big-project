package structure

type Comment struct {
	CommentID string
	ArticleID string
	Message   string
	Username  string
	Fullname  string
}
