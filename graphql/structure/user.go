package structure

type User struct {
	UserID   string
	Username string
	Email    string
	Password string
	FullName string
	Token    string
}
