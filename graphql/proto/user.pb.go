// Code generated by protoc-gen-go. DO NOT EDIT.
// source: user.proto

package proto

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type User struct {
	UserID               string   `protobuf:"bytes,1,opt,name=userID,proto3" json:"userID,omitempty"`
	Username             string   `protobuf:"bytes,2,opt,name=username,proto3" json:"username,omitempty"`
	Email                string   `protobuf:"bytes,3,opt,name=email,proto3" json:"email,omitempty"`
	Password             string   `protobuf:"bytes,4,opt,name=password,proto3" json:"password,omitempty"`
	Fullname             string   `protobuf:"bytes,5,opt,name=fullname,proto3" json:"fullname,omitempty"`
	Token                string   `protobuf:"bytes,6,opt,name=token,proto3" json:"token,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *User) Reset()         { *m = User{} }
func (m *User) String() string { return proto.CompactTextString(m) }
func (*User) ProtoMessage()    {}
func (*User) Descriptor() ([]byte, []int) {
	return fileDescriptor_116e343673f7ffaf, []int{0}
}

func (m *User) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_User.Unmarshal(m, b)
}
func (m *User) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_User.Marshal(b, m, deterministic)
}
func (m *User) XXX_Merge(src proto.Message) {
	xxx_messageInfo_User.Merge(m, src)
}
func (m *User) XXX_Size() int {
	return xxx_messageInfo_User.Size(m)
}
func (m *User) XXX_DiscardUnknown() {
	xxx_messageInfo_User.DiscardUnknown(m)
}

var xxx_messageInfo_User proto.InternalMessageInfo

func (m *User) GetUserID() string {
	if m != nil {
		return m.UserID
	}
	return ""
}

func (m *User) GetUsername() string {
	if m != nil {
		return m.Username
	}
	return ""
}

func (m *User) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *User) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

func (m *User) GetFullname() string {
	if m != nil {
		return m.Fullname
	}
	return ""
}

func (m *User) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

type UserInput struct {
	Username             string   `protobuf:"bytes,1,opt,name=username,proto3" json:"username,omitempty"`
	Email                string   `protobuf:"bytes,2,opt,name=email,proto3" json:"email,omitempty"`
	Password             string   `protobuf:"bytes,3,opt,name=password,proto3" json:"password,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserInput) Reset()         { *m = UserInput{} }
func (m *UserInput) String() string { return proto.CompactTextString(m) }
func (*UserInput) ProtoMessage()    {}
func (*UserInput) Descriptor() ([]byte, []int) {
	return fileDescriptor_116e343673f7ffaf, []int{1}
}

func (m *UserInput) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserInput.Unmarshal(m, b)
}
func (m *UserInput) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserInput.Marshal(b, m, deterministic)
}
func (m *UserInput) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserInput.Merge(m, src)
}
func (m *UserInput) XXX_Size() int {
	return xxx_messageInfo_UserInput.Size(m)
}
func (m *UserInput) XXX_DiscardUnknown() {
	xxx_messageInfo_UserInput.DiscardUnknown(m)
}

var xxx_messageInfo_UserInput proto.InternalMessageInfo

func (m *UserInput) GetUsername() string {
	if m != nil {
		return m.Username
	}
	return ""
}

func (m *UserInput) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *UserInput) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

type UserIDInput struct {
	UserID               string   `protobuf:"bytes,1,opt,name=userID,proto3" json:"userID,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserIDInput) Reset()         { *m = UserIDInput{} }
func (m *UserIDInput) String() string { return proto.CompactTextString(m) }
func (*UserIDInput) ProtoMessage()    {}
func (*UserIDInput) Descriptor() ([]byte, []int) {
	return fileDescriptor_116e343673f7ffaf, []int{2}
}

func (m *UserIDInput) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserIDInput.Unmarshal(m, b)
}
func (m *UserIDInput) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserIDInput.Marshal(b, m, deterministic)
}
func (m *UserIDInput) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserIDInput.Merge(m, src)
}
func (m *UserIDInput) XXX_Size() int {
	return xxx_messageInfo_UserIDInput.Size(m)
}
func (m *UserIDInput) XXX_DiscardUnknown() {
	xxx_messageInfo_UserIDInput.DiscardUnknown(m)
}

var xxx_messageInfo_UserIDInput proto.InternalMessageInfo

func (m *UserIDInput) GetUserID() string {
	if m != nil {
		return m.UserID
	}
	return ""
}

type UserTokenInput struct {
	Token                string   `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UserTokenInput) Reset()         { *m = UserTokenInput{} }
func (m *UserTokenInput) String() string { return proto.CompactTextString(m) }
func (*UserTokenInput) ProtoMessage()    {}
func (*UserTokenInput) Descriptor() ([]byte, []int) {
	return fileDescriptor_116e343673f7ffaf, []int{3}
}

func (m *UserTokenInput) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserTokenInput.Unmarshal(m, b)
}
func (m *UserTokenInput) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserTokenInput.Marshal(b, m, deterministic)
}
func (m *UserTokenInput) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserTokenInput.Merge(m, src)
}
func (m *UserTokenInput) XXX_Size() int {
	return xxx_messageInfo_UserTokenInput.Size(m)
}
func (m *UserTokenInput) XXX_DiscardUnknown() {
	xxx_messageInfo_UserTokenInput.DiscardUnknown(m)
}

var xxx_messageInfo_UserTokenInput proto.InternalMessageInfo

func (m *UserTokenInput) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

type UsernameInput struct {
	Username             string   `protobuf:"bytes,1,opt,name=username,proto3" json:"username,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UsernameInput) Reset()         { *m = UsernameInput{} }
func (m *UsernameInput) String() string { return proto.CompactTextString(m) }
func (*UsernameInput) ProtoMessage()    {}
func (*UsernameInput) Descriptor() ([]byte, []int) {
	return fileDescriptor_116e343673f7ffaf, []int{4}
}

func (m *UsernameInput) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UsernameInput.Unmarshal(m, b)
}
func (m *UsernameInput) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UsernameInput.Marshal(b, m, deterministic)
}
func (m *UsernameInput) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UsernameInput.Merge(m, src)
}
func (m *UsernameInput) XXX_Size() int {
	return xxx_messageInfo_UsernameInput.Size(m)
}
func (m *UsernameInput) XXX_DiscardUnknown() {
	xxx_messageInfo_UsernameInput.DiscardUnknown(m)
}

var xxx_messageInfo_UsernameInput proto.InternalMessageInfo

func (m *UsernameInput) GetUsername() string {
	if m != nil {
		return m.Username
	}
	return ""
}

type CreateUserInput struct {
	Username             string   `protobuf:"bytes,1,opt,name=username,proto3" json:"username,omitempty"`
	Email                string   `protobuf:"bytes,2,opt,name=email,proto3" json:"email,omitempty"`
	Password             string   `protobuf:"bytes,3,opt,name=password,proto3" json:"password,omitempty"`
	Fullname             string   `protobuf:"bytes,4,opt,name=fullname,proto3" json:"fullname,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateUserInput) Reset()         { *m = CreateUserInput{} }
func (m *CreateUserInput) String() string { return proto.CompactTextString(m) }
func (*CreateUserInput) ProtoMessage()    {}
func (*CreateUserInput) Descriptor() ([]byte, []int) {
	return fileDescriptor_116e343673f7ffaf, []int{5}
}

func (m *CreateUserInput) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateUserInput.Unmarshal(m, b)
}
func (m *CreateUserInput) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateUserInput.Marshal(b, m, deterministic)
}
func (m *CreateUserInput) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateUserInput.Merge(m, src)
}
func (m *CreateUserInput) XXX_Size() int {
	return xxx_messageInfo_CreateUserInput.Size(m)
}
func (m *CreateUserInput) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateUserInput.DiscardUnknown(m)
}

var xxx_messageInfo_CreateUserInput proto.InternalMessageInfo

func (m *CreateUserInput) GetUsername() string {
	if m != nil {
		return m.Username
	}
	return ""
}

func (m *CreateUserInput) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func (m *CreateUserInput) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

func (m *CreateUserInput) GetFullname() string {
	if m != nil {
		return m.Fullname
	}
	return ""
}

func init() {
	proto.RegisterType((*User)(nil), "proto.User")
	proto.RegisterType((*UserInput)(nil), "proto.UserInput")
	proto.RegisterType((*UserIDInput)(nil), "proto.UserIDInput")
	proto.RegisterType((*UserTokenInput)(nil), "proto.UserTokenInput")
	proto.RegisterType((*UsernameInput)(nil), "proto.UsernameInput")
	proto.RegisterType((*CreateUserInput)(nil), "proto.CreateUserInput")
}

func init() { proto.RegisterFile("user.proto", fileDescriptor_116e343673f7ffaf) }

var fileDescriptor_116e343673f7ffaf = []byte{
	// 320 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xb4, 0x92, 0xc1, 0x4b, 0x84, 0x40,
	0x14, 0xc6, 0xd3, 0x55, 0x69, 0xdf, 0x56, 0x1b, 0x8f, 0x5a, 0x64, 0x4f, 0x31, 0x50, 0x04, 0xc5,
	0x42, 0xed, 0x21, 0xba, 0x96, 0x04, 0x42, 0xa7, 0xca, 0x43, 0x47, 0x6b, 0xa7, 0x90, 0x5c, 0x47,
	0x46, 0x2d, 0xa2, 0x3f, 0xa4, 0x5b, 0x7f, 0x6b, 0xcc, 0x8c, 0x3b, 0x8e, 0x92, 0x74, 0xda, 0xd3,
	0xee, 0xe7, 0xf7, 0xbe, 0xe7, 0x6f, 0xbe, 0x11, 0xa0, 0x2a, 0x28, 0x9f, 0xe5, 0x9c, 0x95, 0x0c,
	0x5d, 0xf9, 0x43, 0x7e, 0x2c, 0x70, 0xa2, 0x82, 0x72, 0x9c, 0x80, 0x27, 0xdc, 0x30, 0xf0, 0xad,
	0x03, 0xeb, 0x78, 0x78, 0x57, 0x2b, 0x9c, 0xc2, 0xa6, 0xf8, 0x97, 0xc5, 0x4b, 0xea, 0xdb, 0xd2,
	0xd1, 0x1a, 0xf7, 0xc0, 0xa5, 0xcb, 0x38, 0x49, 0xfd, 0x81, 0x34, 0x94, 0x10, 0x89, 0x3c, 0x2e,
	0x8a, 0x0f, 0xc6, 0x17, 0xbe, 0xa3, 0x12, 0x2b, 0x2d, 0xbc, 0x97, 0x2a, 0x4d, 0xe5, 0x36, 0x57,
	0x79, 0x2b, 0x2d, 0xb6, 0x95, 0xec, 0x8d, 0x66, 0xbe, 0xa7, 0xb6, 0x49, 0x41, 0x1e, 0x61, 0x28,
	0xf8, 0xc2, 0x2c, 0xaf, 0xca, 0x16, 0x8c, 0xd5, 0x07, 0x63, 0xf7, 0xc1, 0x0c, 0xda, 0x30, 0xe4,
	0x10, 0x46, 0x72, 0x75, 0xa0, 0x96, 0xf7, 0x34, 0x40, 0x8e, 0x60, 0x47, 0x8c, 0x3d, 0x08, 0x1c,
	0x35, 0xa9, 0x49, 0x2d, 0x93, 0xf4, 0x04, 0xb6, 0xa3, 0x1a, 0xe6, 0x5f, 0x5a, 0xf2, 0x05, 0xe3,
	0x6b, 0x4e, 0xe3, 0x92, 0xae, 0xe9, 0x70, 0xad, 0xa6, 0x9d, 0x76, 0xd3, 0xe7, 0xdf, 0xb6, 0x3a,
	0xf9, 0x3d, 0xe5, 0xef, 0xc9, 0x33, 0xc5, 0x53, 0xd5, 0xf1, 0x2d, 0x7b, 0x4d, 0x32, 0xdc, 0x55,
	0x1f, 0xc8, 0x4c, 0x83, 0x4d, 0x47, 0xc6, 0x13, 0xb2, 0x81, 0x73, 0x80, 0x06, 0x1d, 0x27, 0xb5,
	0xd9, 0x39, 0x4d, 0x37, 0x74, 0x06, 0x5b, 0x37, 0x49, 0xb6, 0x10, 0xea, 0xea, 0x33, 0x0c, 0x10,
	0xcd, 0xb7, 0x04, 0x7f, 0x46, 0x2e, 0x60, 0xdc, 0x44, 0x64, 0xfb, 0xb8, 0x6f, 0x4c, 0x34, 0xf7,
	0xd1, 0x0d, 0x5e, 0x02, 0x36, 0xc1, 0x48, 0x57, 0x68, 0x0c, 0xe9, 0x3b, 0xea, 0x44, 0x9f, 0x3c,
	0xa9, 0xe6, 0xbf, 0x01, 0x00, 0x00, 0xff, 0xff, 0xb2, 0xa2, 0xfb, 0xc8, 0x2a, 0x03, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// UserServiceClient is the client API for UserService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type UserServiceClient interface {
	UserLogin(ctx context.Context, in *UserInput, opts ...grpc.CallOption) (*User, error)
	CreateUser(ctx context.Context, in *CreateUserInput, opts ...grpc.CallOption) (*User, error)
	FindUserByID(ctx context.Context, in *UserIDInput, opts ...grpc.CallOption) (*User, error)
	FindUserByToken(ctx context.Context, in *UserTokenInput, opts ...grpc.CallOption) (*User, error)
	FindUserByUsername(ctx context.Context, in *UsernameInput, opts ...grpc.CallOption) (*User, error)
}

type userServiceClient struct {
	cc *grpc.ClientConn
}

func NewUserServiceClient(cc *grpc.ClientConn) UserServiceClient {
	return &userServiceClient{cc}
}

func (c *userServiceClient) UserLogin(ctx context.Context, in *UserInput, opts ...grpc.CallOption) (*User, error) {
	out := new(User)
	err := c.cc.Invoke(ctx, "/proto.UserService/UserLogin", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userServiceClient) CreateUser(ctx context.Context, in *CreateUserInput, opts ...grpc.CallOption) (*User, error) {
	out := new(User)
	err := c.cc.Invoke(ctx, "/proto.UserService/CreateUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userServiceClient) FindUserByID(ctx context.Context, in *UserIDInput, opts ...grpc.CallOption) (*User, error) {
	out := new(User)
	err := c.cc.Invoke(ctx, "/proto.UserService/FindUserByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userServiceClient) FindUserByToken(ctx context.Context, in *UserTokenInput, opts ...grpc.CallOption) (*User, error) {
	out := new(User)
	err := c.cc.Invoke(ctx, "/proto.UserService/FindUserByToken", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *userServiceClient) FindUserByUsername(ctx context.Context, in *UsernameInput, opts ...grpc.CallOption) (*User, error) {
	out := new(User)
	err := c.cc.Invoke(ctx, "/proto.UserService/FindUserByUsername", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// UserServiceServer is the server API for UserService service.
type UserServiceServer interface {
	UserLogin(context.Context, *UserInput) (*User, error)
	CreateUser(context.Context, *CreateUserInput) (*User, error)
	FindUserByID(context.Context, *UserIDInput) (*User, error)
	FindUserByToken(context.Context, *UserTokenInput) (*User, error)
	FindUserByUsername(context.Context, *UsernameInput) (*User, error)
}

// UnimplementedUserServiceServer can be embedded to have forward compatible implementations.
type UnimplementedUserServiceServer struct {
}

func (*UnimplementedUserServiceServer) UserLogin(ctx context.Context, req *UserInput) (*User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UserLogin not implemented")
}
func (*UnimplementedUserServiceServer) CreateUser(ctx context.Context, req *CreateUserInput) (*User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateUser not implemented")
}
func (*UnimplementedUserServiceServer) FindUserByID(ctx context.Context, req *UserIDInput) (*User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FindUserByID not implemented")
}
func (*UnimplementedUserServiceServer) FindUserByToken(ctx context.Context, req *UserTokenInput) (*User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FindUserByToken not implemented")
}
func (*UnimplementedUserServiceServer) FindUserByUsername(ctx context.Context, req *UsernameInput) (*User, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FindUserByUsername not implemented")
}

func RegisterUserServiceServer(s *grpc.Server, srv UserServiceServer) {
	s.RegisterService(&_UserService_serviceDesc, srv)
}

func _UserService_UserLogin_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserInput)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).UserLogin(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.UserService/UserLogin",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).UserLogin(ctx, req.(*UserInput))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserService_CreateUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateUserInput)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).CreateUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.UserService/CreateUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).CreateUser(ctx, req.(*CreateUserInput))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserService_FindUserByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserIDInput)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).FindUserByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.UserService/FindUserByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).FindUserByID(ctx, req.(*UserIDInput))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserService_FindUserByToken_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserTokenInput)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).FindUserByToken(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.UserService/FindUserByToken",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).FindUserByToken(ctx, req.(*UserTokenInput))
	}
	return interceptor(ctx, in, info, handler)
}

func _UserService_FindUserByUsername_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UsernameInput)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(UserServiceServer).FindUserByUsername(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.UserService/FindUserByUsername",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(UserServiceServer).FindUserByUsername(ctx, req.(*UsernameInput))
	}
	return interceptor(ctx, in, info, handler)
}

var _UserService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.UserService",
	HandlerType: (*UserServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "UserLogin",
			Handler:    _UserService_UserLogin_Handler,
		},
		{
			MethodName: "CreateUser",
			Handler:    _UserService_CreateUser_Handler,
		},
		{
			MethodName: "FindUserByID",
			Handler:    _UserService_FindUserByID_Handler,
		},
		{
			MethodName: "FindUserByToken",
			Handler:    _UserService_FindUserByToken_Handler,
		},
		{
			MethodName: "FindUserByUsername",
			Handler:    _UserService_FindUserByUsername_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "user.proto",
}
