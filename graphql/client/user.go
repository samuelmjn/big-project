package client

import (
	"context"
	"log"

	"gitlab.com/samuelmjn/big-project/graphql/config"
	"gitlab.com/samuelmjn/big-project/graphql/proto"
	"gitlab.com/samuelmjn/big-project/graphql/structure"
	"google.golang.org/grpc"
)

type UserGRPCClient struct {
	Conn proto.UserServiceClient
}

func NewUserGRPCClient() UserClient {
	config.InitPort()
	host := config.UserServiceGRPCServerPort
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatal("couldn't connect to", host, err)
	}
	return &UserGRPCClient{
		Conn: proto.NewUserServiceClient(conn),
	}
}

func (user *UserGRPCClient) UserLogin(username, email, password string) (structure.User, error) {
	userInput := &proto.UserInput{
		Username: username,
		Email:    email,
		Password: password,
	}
	result, err := user.Conn.UserLogin(context.Background(), userInput)
	if err != nil {
		return structure.User{}, err
	}
	resultUser := ParseProtoToUser(result)
	return resultUser, nil
}

func (user *UserGRPCClient) CreateUser(email, username, password, fullname string) (structure.User, error) {
	userInput := &proto.CreateUserInput{
		Email:    email,
		Username: username,
		Password: password,
		Fullname: fullname,
	}
	result, err := user.Conn.CreateUser(context.Background(), userInput)
	if err != nil {
		return structure.User{}, err
	}
	savedUser := ParseProtoToUser(result)
	return savedUser, nil
}

func (user *UserGRPCClient) FindUserByID(id string) (structure.User, error) {
	userIDInput := &proto.UserIDInput{
		UserID: id,
	}
	userResult, err := user.Conn.FindUserByID(context.Background(), userIDInput)
	if err != nil {
		return structure.User{}, err
	}

	result := ParseProtoToUser(userResult)
	return result, nil
}

func (user *UserGRPCClient) FindUserByToken(token string) (structure.User, error) {
	tokenInput := &proto.UserTokenInput{
		Token: token,
	}
	userResult, err := user.Conn.FindUserByToken(context.Background(), tokenInput)
	if err != nil {
		return structure.User{}, err
	}

	result := ParseProtoToUser(userResult)
	return result, nil
}

func (user *UserGRPCClient) FindUserByUsername(username string) (structure.User, error) {
	tokenInput := &proto.UsernameInput{
		Username: username,
	}
	userResult, err := user.Conn.FindUserByUsername(context.Background(), tokenInput)
	if err != nil {
		return structure.User{}, err
	}

	result := ParseProtoToUser(userResult)
	return result, nil
}
