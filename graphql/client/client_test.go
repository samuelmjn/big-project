package client

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCanFindUserById(t *testing.T) {
	//given
	userClient := NewUserGRPCClient()

	//when
	result, err := userClient.FindUserByID("94237f27-aed7-4390-b3bd-80aadcb974ba")

	//then
	assert.Nil(t, err)
	assert.NotNil(t, result)

}
