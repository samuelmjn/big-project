package client

import (
	"gitlab.com/samuelmjn/big-project/graphql/proto"
	"gitlab.com/samuelmjn/big-project/graphql/structure"
)

func ParseProtoToUser(pb *proto.User) structure.User {
	return structure.User{
		UserID:   pb.UserID,
		Username: pb.Username,
		Email:    pb.Email,
		FullName: pb.Fullname,
		Token:    pb.Token,
	}
}
