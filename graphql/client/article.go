package client

import (
	"context"
	"log"

	"gitlab.com/samuelmjn/big-project/graphql/structure"

	"gitlab.com/samuelmjn/big-project/graphql/config"
	"gitlab.com/samuelmjn/big-project/graphql/proto"
	"google.golang.org/grpc"
)

type ArticleGRPCClient struct {
	Conn proto.ArticleServiceClient
}

//NewArticleGRPCClient new client
func NewArticleGRPCClient() ArticleClient {
	config.InitPort()
	host := config.ArticleServiceGRPCServerPort
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatal("couldn't connect to", host, err)
	}
	return &ArticleGRPCClient{
		Conn: proto.NewArticleServiceClient(conn),
	}
}

func (articleClient *ArticleGRPCClient) GetPublishedArticlesByCategory(category string, offset int32, limit int32) ([]structure.Article, error) {
	categoryInput := proto.ArticleGetByCategoryRequest{
		Category: category,
		Offset:   offset,
		Limit:    limit,
	}
	result, err := articleClient.Conn.GetPublishedArticlesByCategory(context.Background(), &categoryInput)
	if err != nil {
		return []structure.Article{}, err
	}

	resultArticles := make([]structure.Article, len(result.Article))
	for k, v := range result.Article {
		article, _ := ParseProtoToArticle(*v)
		resultArticles[k] = article
	}
	return resultArticles, nil
}

func (articleClient *ArticleGRPCClient) GetArticleBySlug(slug string) (structure.Article, error) {
	article := structure.Article{}
	slugInput := proto.ArticleGetBySlugRequest{
		Slug: slug,
	}
	result, err := articleClient.Conn.GetArticleBySlug(context.Background(), &slugInput)
	if err != nil {
		return structure.Article{}, err
	}
	resultArticle, _ := ParseProtoToArticle(*result)
	article = resultArticle
	return article, nil
}

func (articleClient *ArticleGRPCClient) CreateArticle(title, slug, body, thumbnail, userId, status string, categories []string) (structure.Article, error) {
	articleInput := proto.CreateArticleRequest{
		Title:      title,
		Slug:       slug,
		Body:       body,
		Thumbnail:  thumbnail,
		UserId:     userId,
		Status:     status,
		Categories: categories,
	}
	result, err := articleClient.Conn.CreateArticle(context.Background(), &articleInput)
	if err != nil {
		return structure.Article{}, err
	}
	resultArticle, err := ParseProtoToArticle(*result)
	return resultArticle, nil
}

func (articleClient *ArticleGRPCClient) FindPublishedArticles(keyword string, offset int32, limit int32) ([]structure.Article, error) {
	searchInput := proto.SearchInput{
		Keyword: keyword,
		Offset:  offset,
		Limit:   limit,
	}
	result, err := articleClient.Conn.FindPublishedArticles(context.Background(), &searchInput)
	if err != nil {
		return []structure.Article{}, err
	}

	resultArticles := make([]structure.Article, len(result.Article))
	for k, v := range result.Article {
		article, _ := ParseProtoToArticle(*v)
		resultArticles[k] = article
	}
	return resultArticles, nil
}

func (articleClient *ArticleGRPCClient) CreateUserCategory(userid, categoryname, categoryslug string) (structure.Category, error) {
	result, err := articleClient.Conn.CreateUserCategory(context.Background(), &proto.CreateUserCategoryInput{Userid: userid, Categoryname: categoryname, Categoryslug: categoryslug})
	if err != nil {
		return structure.Category{}, err
	}

	category := structure.Category{
		CategoryID:   result.Categoryid,
		UserID:       result.Userid,
		Categoryname: result.Categoryname,
		Categoryslug: result.Categoryslug,
	}

	return category, nil
}

func (articleClient *ArticleGRPCClient) GetUserCategoriesByUserid(userid string, offset int32, limit int32) ([]structure.Category, error) {
	result, err := articleClient.Conn.GetUserCategoriesByUserid(context.Background(), &proto.GetUserCategoryInput{Userid: userid, Offset: offset, Limit: limit})
	if err != nil {
		return []structure.Category{}, err
	}

	categories := make([]structure.Category, len(result.UserCategory))
	for k, v := range result.UserCategory {
		category := structure.Category{
			CategoryID:   v.Categoryid,
			UserID:       v.Userid,
			Categoryname: v.Categoryname,
			Categoryslug: v.Categoryslug,
		}
		categories[k] = category
	}
	return categories, nil
}

func (articleClient *ArticleGRPCClient) UpdateArticle(articleID, userID, title, slug, body, thumbnail, status string, categories []string) (structure.Article, error) {
	articleInput := proto.CreateArticleRequest{
		Id:         articleID,
		Title:      title,
		Slug:       slug,
		Body:       body,
		Thumbnail:  thumbnail,
		UserId:     userID,
		Status:     status,
		Categories: categories,
	}
	result, err := articleClient.Conn.UpdateArticle(context.Background(), &articleInput)
	if err != nil {
		return structure.Article{}, err
	}
	resultArticle, err := ParseProtoToArticle(*result)
	return resultArticle, nil
}

func (articleClient *ArticleGRPCClient) ProfileArticles(userID, category string, offset int32, limit int32) ([]structure.Article, error) {
	profileRequest := proto.ProfileArticlesRequest{
		Userid:   userID,
		Category: category,
		Offset:   offset,
		Limit:    limit,
	}
	result, err := articleClient.Conn.ProfileArticles(context.Background(), &profileRequest)
	if err != nil {
		return []structure.Article{}, err
	}

	resultArticles := make([]structure.Article, len(result.Article))
	for k, v := range result.Article {
		article, _ := ParseProtoToArticle(*v)
		resultArticles[k] = article
	}
	return resultArticles, nil
}

func (articleClient *ArticleGRPCClient) DashboardArticles(userID, keyword, category, status string, offset int32, limit int32) ([]structure.Article, error) {
	dashboardRequest := proto.DashboardArticlesRequest{
		Userid:   userID,
		Keyword:  keyword,
		Category: category,
		Status:   status,
		Offset:   offset,
		Limit:    limit,
	}
	result, err := articleClient.Conn.DashboardArticles(context.Background(), &dashboardRequest)
	if err != nil {
		return []structure.Article{}, err
	}

	resultArticles := make([]structure.Article, len(result.Article))
	for k, v := range result.Article {
		article, _ := ParseProtoToArticle(*v)
		resultArticles[k] = article
	}
	return resultArticles, nil
}
