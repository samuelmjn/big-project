package client

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// func TestCanGetPublishedArticles(t *testing.T) {
// 	//given
// 	articleClient := NewArticleGRPCClient()

// 	//when
// 	result, err := articleClient.GetPublishedArticles()

// 	//then
// 	assert.Nil(t, err)
// 	assert.NotNil(t, result)
// }

// func TestCanCreateArticle(t *testing.T) {
// 	//given
// 	articleClient := NewArticleGRPCClient()

// 	//when
// 	result, err := articleClient.CreateArticle("aa", "aa", "bb", "tb", "aaa", "DRAFT", "aaa-aaa")

// 	//then
// 	assert.Nil(t, err)
// 	assert.Equal(t, "tb", result.Thumbnail)
// }

func TestCanGetPublishedArticlesByCategory(t *testing.T) {
	//given
	articleClient := NewArticleGRPCClient()

	//when
	result, err := articleClient.GetPublishedArticlesByCategory("NEWS", 0, 10)

	//then
	assert.Nil(t, err)
	assert.NotNil(t, result[0].Slug)
}

func TestCanGetUserCategoriesByUserid(t *testing.T) {
	//
	articleClient := NewArticleGRPCClient()

	//when
	result, err := articleClient.GetUserCategoriesByUserid("157134ef-2bbd-476a-bcfe-e286f49d7e38", 0, 10)

	assert.Nil(t, err)
	assert.NotNil(t, result)
}

func TestCanCreateUserCategory(t *testing.T) {
	articleClient := NewArticleGRPCClient()

	//when
	result, err := articleClient.CreateUserCategory("157134ef-2bbd-476a-bcfe-e286f49d7e38", "BUKUU", "FAJRI-BUKUU")

	assert.Nil(t, err)
	assert.Equal(t, "157134ef-2bbd-476a-bcfe-e286f49d7e38", result.UserID)
}

func TestCanGetProfileArticles(t *testing.T) {
	//given
	articleClient := NewArticleGRPCClient()

	//when
	result, err := articleClient.ProfileArticles("aaa", "NEWS", 0, 10)

	//then
	assert.Nil(t, err)
	assert.NotNil(t, result)
}

func TestCanGetDashboardArticles(t *testing.T) {
	//given
	articleClient := NewArticleGRPCClient()

	//when
	result, err := articleClient.DashboardArticles("aaa", "", "NEWS", "PUBLISHED", 0, 10)

	//then
	assert.Nil(t, err)
	assert.NotNil(t, result)
}

func TestCanCreateArticles(t *testing.T) {
	//given
	articleClient := NewArticleGRPCClient()
	cat := []string{"1", "2", "3"}
	//when
	result, err := articleClient.CreateArticle("ini title2", "", "ini body", "http://res.cloudinary.com/duv3dli2f/image/upload/v1565134283/imageblob/cat-20190807063120.png", "c2b7ebcb-d2ad-4740-a649-dc1236431f4f", "PUBLISHED", cat)

	//then
	assert.Nil(t, err)
	assert.NotNil(t, result)
}

func TestCanGetArticleBySlug(t *testing.T) {
	//given
	articleClient := NewArticleGRPCClient()
	//when
	result, err := articleClient.GetArticleBySlug("ini-title1-cafaf6a5")

	//then
	assert.Nil(t, err)
	assert.NotNil(t, result)
}
