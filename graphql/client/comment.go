package client

import (
	"context"
	"log"

	"gitlab.com/samuelmjn/big-project/graphql/config"
	"gitlab.com/samuelmjn/big-project/graphql/proto"
	"gitlab.com/samuelmjn/big-project/graphql/structure"
	"google.golang.org/grpc"
)

type CommentGRPCClient struct {
	Conn proto.CommentServiceClient
}

func NewCommentGRPCClient() CommentClient {
	config.InitPort()
	host := config.CommentServiceGRPCServerPort
	conn, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		log.Fatal("couldn't connect to", host, err)
	}
	return &CommentGRPCClient{
		Conn: proto.NewCommentServiceClient(conn),
	}
}

func (comment *CommentGRPCClient) CreateComment(message, username, fullname, articleid string) (structure.Comment, error) {
	commentInput := &proto.CreateCommentInput{
		Message:   message,
		Username:  username,
		Fullname:  fullname,
		ArticleID: articleid,
	}
	result, err := comment.Conn.CreateComment(context.Background(), commentInput)
	if err != nil {
		return structure.Comment{}, err
	}
	savedComment := ParseProtoToComment(result)
	return savedComment, nil
}

func (comment *CommentGRPCClient) GetComments(articleID string, limit, offset int32) ([]structure.Comment, error) {
	comments := []structure.Comment{}
	commentInput := &proto.GetCommentInput{
		ArticleID: articleID,
		Offset:    offset,
		Limit:     limit,
	}
	result, err := comment.Conn.GetComments(context.Background(), commentInput)
	if err != nil {
		return []structure.Comment{}, err
	}

	for _, v := range result.Comment {
		comment := ParseProtoToComment(v)
		comments = append(comments, comment)
	}
	return comments, nil
}

func (comment *CommentGRPCClient) GetCommentsCount(articleID string) int32 {
	commentInput := &proto.GetCommentInput{
		ArticleID: articleID,
	}
	result, err := comment.Conn.GetCommentsCount(context.Background(), commentInput)
	if err != nil {
		return 0
	}

	count := result.CommentCount
	return count
}
