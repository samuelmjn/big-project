package client

import (
	"gitlab.com/samuelmjn/big-project/graphql/proto"
	"gitlab.com/samuelmjn/big-project/graphql/structure"
)

func ParseProtoToComment(pb *proto.Comment) structure.Comment {
	return structure.Comment{
		CommentID: pb.CommentID,
		ArticleID: pb.ArticleID,
		Message:   pb.Message,
		Username:  pb.Username,
		Fullname:  pb.Fullname,
	}
}
