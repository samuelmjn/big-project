package client

import (
	"gitlab.com/samuelmjn/big-project/graphql/structure"
)

type UserClient interface {
	UserLogin(username, email, password string) (structure.User, error)
	FindUserByID(id string) (structure.User, error)
	FindUserByToken(token string) (structure.User, error)
	CreateUser(email, username, password, fullname string) (structure.User, error)
	FindUserByUsername(username string) (structure.User, error)
}

type ArticleClient interface {
	// GetPublishedArticlesByCategory(category string, offset int32, limit int32) ([]structure.Article, error)
	// GetPublishedArticleBySlug(slug string) (structure.Article, error)
	// FindPublishedArticles(keyword string) ([]structure.Article, error)
	// CreateArticle(title, slug, body, thumbnail, userId, status string, categories []string) (structure.Article, error)
	// CreateUserCategory(username, categoryname, categoryslug string) (structure.Category, error)
	// GetUserCategoriesByUsername(username string) ([]structure.Category, error)
	// UpdateArticle(articleID, userID, title, slug, body, thumbnail, status, categories string) (structure.Article, error)
	// ProfileArticles(userID, category string) ([]structure.Article, error)
	// DashboardArticles(userID, keyword, category, status string) ([]structure.Article, error)

	FindPublishedArticles(keyword string, offset int32, limit int32) ([]structure.Article, error)
	GetPublishedArticlesByCategory(category string, offset int32, limit int32) ([]structure.Article, error)
	GetArticleBySlug(slug string) (structure.Article, error)
	CreateArticle(title, slug, body, thumbnail, userId, status string, categories []string) (structure.Article, error)
	CreateUserCategory(userid, categoryname, categoryslug string) (structure.Category, error)
	GetUserCategoriesByUserid(userid string, offset int32, limit int32) ([]structure.Category, error)
	UpdateArticle(articleID, userID, title, slug, body, thumbnail, status string, categories []string) (structure.Article, error)
	ProfileArticles(userID string, category string, offset int32, limit int32) ([]structure.Article, error)
	DashboardArticles(userID, keyword, category, status string, offset int32, limit int32) ([]structure.Article, error)
}

type CommentClient interface {
	GetComments(articleID string, limit, offset int32) ([]structure.Comment, error)
	GetCommentsCount(articleID string) int32
	CreateComment(message, username, fullname, articleid string) (structure.Comment, error)
}
