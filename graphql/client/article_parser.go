package client

import (
	"github.com/golang/protobuf/ptypes"

	"gitlab.com/samuelmjn/big-project/graphql/proto"
	"gitlab.com/samuelmjn/big-project/graphql/structure"
)

func ParseProtoToArticle(article proto.Article) (structure.Article, error) {
	createdAt, _ := ptypes.Timestamp(article.CreatedAt)
	updatedAt, _ := ptypes.Timestamp(article.UpdatedAt)
	var categories []structure.Category
	for _, v := range article.Categories {
		cat := structure.Category{
			UserID:       v.Userid,
			CategoryID:   v.Categoryid,
			Categoryname: v.Categoryname,
			Categoryslug: v.Categoryslug,
		}
		categories = append(categories, cat)
	}
	return structure.Article{
		Id:         article.Id,
		Title:      article.Title,
		Slug:       article.Slug,
		Body:       article.Body,
		Status:     article.Status,
		CreatedAt:  createdAt,
		UpdatedAt:  updatedAt,
		Thumbnail:  article.Thumbnail,
		Categories: categories,
		UserID:     article.UserId,
	}, nil
}
