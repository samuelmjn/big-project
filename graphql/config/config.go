package config

import (
	"os"

	"github.com/joho/godotenv"
)

var (
	ENV string
	// UserServiceGRPCServerPort    string = "user:9100"
	// ArticleServiceGRPCServerPort string = "article:9200"
	// CommentServiceGRPCServerPort string = "comment:9300"
	UserServiceGRPCServerPort    string = ":9100"
	ArticleServiceGRPCServerPort string = ":9200"
	CommentServiceGRPCServerPort string = ":9300"
)

func InitPort() {
	godotenv.Load()
	// Check the environment variable
	env := os.Getenv("ENV")
	if env != "production" {
		ENV = "development"
		UserServiceGRPCServerPort = ":9100"
		ArticleServiceGRPCServerPort = ":9200"
		CommentServiceGRPCServerPort = ":9300"

	} else {
		UserServiceGRPCServerPort = "user:9100"
		ArticleServiceGRPCServerPort = "article:9200"
		CommentServiceGRPCServerPort = "comment:9300"
		ENV = "production"
	}
}
