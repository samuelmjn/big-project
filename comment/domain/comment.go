package domain

import (
	"errors"

	uuid "github.com/satori/go.uuid"
)

type Comment struct {
	CommentID string
	ArticleID string
	Message   string
	Username  string
	Fullname  string
}

func CreateComment(message, username, fullname, articleid string) (Comment, error) {
	if message == "" {
		return Comment{}, errors.New("Komentar tidak boleh kosong")
	}
	return Comment{
		CommentID: uuid.NewV4().String(),
		ArticleID: articleid,
		Message:   message,
		Username:  username,
		Fullname:  fullname,
	}, nil
}
