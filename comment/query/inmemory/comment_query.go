package inmemory

import (
	"errors"

	"gitlab.com/samuelmjn/big-project/comment/domain"
	"gitlab.com/samuelmjn/big-project/comment/query"
	"gitlab.com/samuelmjn/big-project/comment/storage"
)

type CommentQueryInMemory struct {
	Storage *storage.CommentStorage
}

func NewCommentQueryInMemory(storage *storage.CommentStorage) query.CommentQuery {
	return &CommentQueryInMemory{Storage: storage}
}

func (q *CommentQueryInMemory) GetComments(articleID string, offset, limit int32) query.QueryResult {
	commentsList := []domain.Comment{}
	for _, comment := range q.Storage.CommentMap {
		if comment.ArticleID == articleID {
			commentsList = append(commentsList, comment)
		}
	}
	if len(commentsList) == 0 {
		return query.QueryResult{Result: commentsList, Error: errors.New("No Comments")}
	}

	return query.QueryResult{Result: commentsList, Error: nil}
}
