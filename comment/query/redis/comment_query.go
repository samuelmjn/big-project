package redis

import (
	"encoding/json"
	"errors"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/samuelmjn/big-project/comment/config"
	"gitlab.com/samuelmjn/big-project/comment/domain"
)

func GetCommentsCache(articleID string) ([]domain.Comment, error) {
	comments := []domain.Comment{}
	redisPool := config.InitRedis()
	conn := redisPool.Get()
	defer conn.Close()
	result, err := redis.String(conn.Do("GET", articleID))
	if err == nil {
		err = json.Unmarshal([]byte(result), &comments)
		if err != nil {
			return comments, err
		}
	} else if err == redis.ErrNil {
		return comments, errors.New("not found")
	} else if err != nil {
		return comments, err
	}

	return comments, nil
}
