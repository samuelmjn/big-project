package query

type QueryResult struct {
	Result interface{}
	Error  error
}

type CommentQuery interface {
	GetComments(articleID string, offset, limit int32) QueryResult
}
