package cockroach

import (
	"database/sql"

	"gitlab.com/samuelmjn/big-project/comment/domain"

	"gitlab.com/samuelmjn/big-project/comment/query"
)

type CommentQueryCockroach struct {
	DB *sql.DB
}

func NewCommentQueryCockroach(DB *sql.DB) query.CommentQuery {
	return &CommentQueryCockroach{DB: DB}
}

func (q *CommentQueryCockroach) GetComments(articleID string, offset, limit int32) query.QueryResult {
	comments := []domain.Comment{}
	rows, err := q.DB.Query(`SELECT * FROM "COMMENT" WHERE "ARTICLEID" = $1 OFFSET $2 LIMIT $3`, articleID, offset, limit)
	if err != nil {
		return query.QueryResult{Result: comments, Error: err}
	}
	for rows.Next() {
		comment := domain.Comment{}
		err := rows.Scan(&comment.CommentID, &comment.ArticleID, &comment.Message, &comment.Username, &comment.Fullname)
		if err != nil {
			return query.QueryResult{Result: comments, Error: err}
		}
		comments = append(comments, comment)
	}
	return query.QueryResult{Result: comments, Error: nil}
}
