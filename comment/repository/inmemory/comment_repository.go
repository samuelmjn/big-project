package inmemory

import (
	"gitlab.com/samuelmjn/big-project/comment/domain"
	"gitlab.com/samuelmjn/big-project/comment/repository"
	"gitlab.com/samuelmjn/big-project/comment/storage"
)

type CommentRepositoryInMemory struct {
	Storage *storage.CommentStorage
}

func NewCommentRepositoryInMemory(storage *storage.CommentStorage) repository.CommentRepository {
	return &CommentRepositoryInMemory{Storage: storage}
}

func (repo *CommentRepositoryInMemory) SaveComment(comment *domain.Comment) error {
	repo.Storage.CommentMap = append(repo.Storage.CommentMap, *comment)
	return nil
}
