package redis

import (
	"encoding/json"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/samuelmjn/big-project/comment/config"
	"gitlab.com/samuelmjn/big-project/comment/domain"
)

func CreateCommentCache(comment domain.Comment, articleID string) error {
	comments := []domain.Comment{}
	redisPool := config.InitRedis()
	conn := redisPool.Get()
	defer conn.Close()
	result, err := redis.String(conn.Do("GET", articleID))
	if err == nil {
		err = json.Unmarshal([]byte(result), &comments)
		if err != nil {
			return err
		}
	}

	comments = append(comments, comment)
	json, err := json.Marshal(comments)
	if err != nil {
		return err
	}

	_, err = conn.Do("SET", comment.ArticleID, json)
	if err != nil {
		return err
	}

	return nil
}
