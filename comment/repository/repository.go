package repository

import "gitlab.com/samuelmjn/big-project/comment/domain"

type CommentRepository interface {
	SaveComment(comment *domain.Comment) error
}
