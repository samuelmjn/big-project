package cockroach

import (
	"database/sql"

	"gitlab.com/samuelmjn/big-project/comment/domain"
	"gitlab.com/samuelmjn/big-project/comment/repository"
)

type CommentRepositoryCockroach struct {
	DB *sql.DB
}

func NewCommentRepositoryCockroach(DB *sql.DB) repository.CommentRepository {
	return &CommentRepositoryCockroach{DB: DB}
}

func (r *CommentRepositoryCockroach) SaveComment(comment *domain.Comment) error {
	_, err := r.DB.Exec(`INSERT INTO "COMMENT" ("COMMENTID", "ARTICLEID", "MESSAGE", "USERNAME", "FULLNAME") VALUES ($1,$2,$3,$4,$5)`,
		comment.CommentID,
		comment.ArticleID,
		comment.Message,
		comment.Username,
		comment.Fullname)

	if err != nil {
		return err
	}

	return nil
}
