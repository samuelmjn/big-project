package container

import (
	"gitlab.com/samuelmjn/big-project/comment/config"
	"gitlab.com/samuelmjn/big-project/comment/query"
	cq "gitlab.com/samuelmjn/big-project/comment/query/cockroach"
	iq "gitlab.com/samuelmjn/big-project/comment/query/inmemory"
	"gitlab.com/samuelmjn/big-project/comment/repository"
	cr "gitlab.com/samuelmjn/big-project/comment/repository/cockroach"
	ir "gitlab.com/samuelmjn/big-project/comment/repository/inmemory"
	"gitlab.com/samuelmjn/big-project/comment/storage"
)

type Comment struct {
	Query      query.CommentQuery
	Repository repository.CommentRepository
}

type App struct {
	Comment Comment
}

func InitApp() *App {
	driver := ""
	// driver := "cockroach"
	switch driver {
	case "cockroach":
		db := config.InitCockroach()

		return &App{
			Comment: Comment{
				Query:      cq.NewCommentQueryCockroach(db),
				Repository: cr.NewCommentRepositoryCockroach(db),
			},
		}
	default:
		db := storage.NewCommentStorage()
		return &App{
			Comment: Comment{
				Query:      iq.NewCommentQueryInMemory(db),
				Repository: ir.NewCommentRepositoryInMemory(db),
			},
		}
	}
}
