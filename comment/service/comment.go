package service

import (
	"log"

	"gitlab.com/samuelmjn/big-project/comment/container"
	"gitlab.com/samuelmjn/big-project/comment/domain"
	"gitlab.com/samuelmjn/big-project/comment/query"
	qr "gitlab.com/samuelmjn/big-project/comment/query/redis"
	"gitlab.com/samuelmjn/big-project/comment/repository"
	rr "gitlab.com/samuelmjn/big-project/comment/repository/redis"
)

type CommentService struct {
	Query      query.CommentQuery
	Repository repository.CommentRepository
}

func NewCommentService(app *container.App) *CommentService {
	return &CommentService{
		Query:      app.Comment.Query,
		Repository: app.Comment.Repository,
	}
}

func (s *CommentService) GetComments(articleID string, limit, offset int32) ([]domain.Comment, error) {
	comments := []domain.Comment{}
	comments, err := qr.GetCommentsCache(articleID)
	if err != nil {
		comments := s.Query.GetComments(articleID, limit, offset)
		if comments.Error != nil {
			return []domain.Comment{}, comments.Error
		} else {
			return comments.Result.([]domain.Comment), nil
		}
	}
	return comments, nil
}

func (s *CommentService) GetCommentsCount(articleID string) int {
	comments := s.Query.GetComments(articleID, 0, 0)
	commentsCount := len(comments.Result.([]domain.Comment))
	if comments.Error != nil {
		return 0
	} else {
		return commentsCount
	}
}

func (s *CommentService) CreateComment(message, username, fullname, articleid string) (domain.Comment, error) {
	comment, err := domain.CreateComment(message, username, fullname, articleid)
	if err != nil {
		log.Println(err)
		return domain.Comment{}, err
	}
	err = s.Repository.SaveComment(&comment)
	if err != nil {
		return domain.Comment{}, err
	}
	errCache := rr.CreateCommentCache(comment, articleid)
	if errCache != nil {
		return domain.Comment{}, err
	}
	return comment, nil
}
