package server

import (
	"gitlab.com/samuelmjn/big-project/comment/domain"
	"gitlab.com/samuelmjn/big-project/comment/proto"
)

func ParseCommentToProto(comment domain.Comment) proto.Comment {
	return proto.Comment{
		CommentID: comment.CommentID,
		ArticleID: comment.ArticleID,
		Message:   comment.Message,
		Username:  comment.Username,
		Fullname:  comment.Fullname,
	}
}
