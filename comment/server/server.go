package server

import (
	"context"

	"gitlab.com/samuelmjn/big-project/comment/domain"
	"gitlab.com/samuelmjn/big-project/comment/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

type CommentServerService interface {
	GetComments(articleID string, limit, offset int32) ([]domain.Comment, error)
	GetCommentsCount(articleID string) int
	CreateComment(message, username, fullname, articleid string) (domain.Comment, error)
}

type CommentServer struct {
	CommentService CommentServerService
}

func NewCommentServer(services CommentServerService) proto.CommentServiceServer {
	return &CommentServer{
		CommentService: services,
	}
}

func (s *CommentServer) GetComments(ctx context.Context, params *proto.GetCommentInput) (*proto.CommentList, error) {
	articleId := params.GetArticleID()
	limit := params.GetLimit()
	offset := params.GetOffset()
	result := proto.CommentList{}

	comments, err := s.CommentService.GetComments(articleId, limit, offset)
	if err != nil {
		return &proto.CommentList{}, err
	}
	protoComments := []*proto.Comment{}
	for _, comment := range comments {
		protoComment := ParseCommentToProto(comment)
		protoComments = append(protoComments, &protoComment)
	}
	result.Comment = protoComments

	return &result, nil
}

func (s *CommentServer) GetCommentsCount(ctx context.Context, params *proto.GetCommentInput) (*proto.CommentCount, error) {
	articleId := params.GetArticleID()
	commentCount := s.CommentService.GetCommentsCount(articleId)
	result := proto.CommentCount{CommentCount: int32(commentCount)}

	return &result, nil
}

func (s *CommentServer) CreateComment(ctx context.Context, params *proto.CreateCommentInput) (*proto.Comment, error) {
	result := proto.Comment{}
	message := params.GetMessage()
	fullname := params.GetFullname()
	articleId := params.GetArticleID()
	username := params.GetUsername()
	comment, err := s.CommentService.CreateComment(message, username, fullname, articleId)
	if err != nil {
		return &result, grpc.Errorf(codes.FailedPrecondition, err.Error())
	}

	commentProto := ParseCommentToProto(comment)
	result = commentProto
	return &result, nil
}
