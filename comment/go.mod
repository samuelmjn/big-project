module gitlab.com/samuelmjn/big-project/comment

go 1.12

require (
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/golang/protobuf v1.3.2
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.2.0
	github.com/mailru/easyjson v0.0.0-20190626092158-b2ccc519800e // indirect
	github.com/olivere/elastic v6.2.22+incompatible
	github.com/pkg/errors v0.8.1 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.3.0
	google.golang.org/grpc v1.22.1
)
