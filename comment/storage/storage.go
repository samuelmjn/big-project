package storage

import "gitlab.com/samuelmjn/big-project/comment/domain"

type CommentStorage struct {
	CommentMap []domain.Comment
}

func NewCommentStorage() *CommentStorage {
	samnpleComment := &domain.Comment{
		CommentID: "265a7a5a-7aec-4d3f-a378-5082b349f58e",
		ArticleID: "ed19e7df-35ab-4baf-a17d-2f2989e7c93f",
		Message:   "mantul gan",
		Username:  "bambang",
		Fullname:  "Bambang Setyanto",
	}
	return &CommentStorage{
		CommentMap: []domain.Comment{*samnpleComment},
	}
}
