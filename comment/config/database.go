package config

import (
	"database/sql"
	"log"

	"github.com/gomodule/redigo/redis"
	_ "github.com/lib/pq"
)

func InitCockroach() *sql.DB {
	dsn := "postgresql://root@database:26257/comment?sslmode=disable"
	db, err := sql.Open("postgres", dsn)
	log.Println(err)

	return db
	// dsn := "postgresql://root@database:26257/sslmode=disable"
	// db, err := sql.Open("postgres", dsn)
	// db.Exec(`CREATE DATABASE IF NOT EXISTS "comment"`)
	// db.Close()

	// dsn = "postgresql://root@database:26257/comment?sslmode=disable"
	// db, err = sql.Open("postgres", dsn)
	// if err != nil {
	// 	panic(err)
	// }
	// ddl, err := ioutil.ReadFile("config/ddl.sql")
	// if err != nil {
	// 	fmt.Println(err)
	// 	panic("Error, cannot run ddl")
	// }
	// sql := string(ddl)
	// _, err = db.Exec(sql)
	// if err != nil {
	// 	panic(err)
	// }
	// return db
}

func InitRedis() *redis.Pool {
	return &redis.Pool{
		// Maximum number of idle connections in the pool.
		MaxIdle: 80,
		// max number of connections
		MaxActive: 12000,
		// Dial is an application supplied function for creating and
		// configuring a connection.
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", "redis:6379")
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}
}
