package config

import (
	"os"

	"github.com/joho/godotenv"
)

var (
	// ENV store environment variable
	ENV string

	// GRPCArticleServerPort port for serving article service
	CommentServiceGRPCServerPort = "comment:9300"
)

//InitPort to load env for port
func InitPort() {
	godotenv.Load()

	// Check the environment variable
	env := os.Getenv("ENV")
	if env != "production" {
		ENV = "development"
		CommentServiceGRPCServerPort = ":9300"

	} else {
		CommentServiceGRPCServerPort = "comment:9300"
		ENV = "production"
	}
}
