package main

import (
	"log"
	"net"

	"gitlab.com/samuelmjn/big-project/comment/config"
	"gitlab.com/samuelmjn/big-project/comment/container"
	"gitlab.com/samuelmjn/big-project/comment/proto"
	"gitlab.com/samuelmjn/big-project/comment/server"
	"gitlab.com/samuelmjn/big-project/comment/service"
	"google.golang.org/grpc"
)

func main() {
	config.InitPort()
	srv := grpc.NewServer()
	app := container.InitApp()
	service := service.NewCommentService(app)
	CommentServer := server.NewCommentServer(service)
	proto.RegisterCommentServiceServer(srv, CommentServer)
	log.Println("Starting gRPC Server at", config.CommentServiceGRPCServerPort)
	listen, err := net.Listen("tcp", config.CommentServiceGRPCServerPort)
	if err != nil {
		log.Fatalf("could not listen to %s: %v", config.CommentServiceGRPCServerPort, err)
	}

	log.Fatal("Server is listening", srv.Serve(listen))
}
