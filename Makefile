run-comment:
	@cd comment && go run .

run-article:
	@cd article && go run .

run-user:
	@cd user && go run .

run-graphql:
	@cd graphql && go run .

env-dev:
	@go run gen-env.go development

env-prod:
	@go run gen-env.go production
	
