package storage

import (
	"time"

	"gitlab.com/samuelmjn/big-project/article/domain"
)

//ArticleStorage is to wrap ArticleMap
type ArticleStorage struct {
	ArticleMap      []domain.Article
	CategoryMap     map[string]string
	UserCategoryMap []domain.Category
}

//CreateArticleStorage is for injecting inmemory storage
func CreateArticleStorage() *ArticleStorage {
	var articleMap []domain.Article
	articleMap = append(articleMap, InitSampleArticle()...)
	var categoryMap map[string]string
	categoryMap = InitCategorySample()
	var userCategory []domain.Category
	userCategory = InitUserCategory()
	return &ArticleStorage{
		ArticleMap:      articleMap,
		CategoryMap:     categoryMap,
		UserCategoryMap: userCategory,
	}
}

//InitSampleArticle is init sample data in storage
func InitSampleArticle() []domain.Article {
	article1 := domain.Article{
		ID:         "e56fa536-982f-4cc7-ab3b-e9d45f050a59",
		Title:      "Berita Sample satu",
		Slug:       "berita-sample-1",
		Body:       "{\"blocks\":[{\"key\":\"5fjf8\",\"text\":\"This is just its content, with a bold and italic but without underline\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":33,\"length\":5,\"style\":\"BOLD\"},{\"offset\":42,\"length\":6,\"style\":\"ITALIC\"},{\"offset\":61,\"length\":9,\"style\":\"UNDERLINE\"}],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}",
		Status:     "PUBLISHED",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Categories: "NEWS;POLITIK",
		Thumbnail:  "https://image-store.slidesharecdn.com/084231bb-98d6-476e-ac6f-06da2abf8cfe-original.jpeg",
		UserID:     "9237f27-aed7-4390-b3bd-80aadcb974ba",
	}

	article2 := domain.Article{
		ID:         "2e0f5b60-8848-428a-ab62-f79cad562286 ",
		Title:      "Berita Sample dua",
		Slug:       "berita-sample-2",
		Body:       "{\"blocks\":[{\"key\":\"5fjf8\",\"text\":\"This is just its content, with a bold and italic but without underline\",\"type\":\"unstyled\",\"depth\":0,\"inlineStyleRanges\":[{\"offset\":33,\"length\":5,\"style\":\"BOLD\"},{\"offset\":42,\"length\":6,\"style\":\"ITALIC\"},{\"offset\":61,\"length\":9,\"style\":\"UNDERLINE\"}],\"entityRanges\":[],\"data\":{}}],\"entityMap\":{}}",
		Status:     "PUBLISHED",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Categories: "NEWS;ENTERTAINMENT",
		Thumbnail:  "https://image-store.slidesharecdn.com/084231bb-98d6-476e-ac6f-06da2abf8cfe-original.jpeg",
		UserID:     "92237f27-aed7-4390-b3bd-80aadcb974ba",
	}
	articles := []domain.Article{
		article1,
		article2,
	}
	return articles
}

func InitCategorySample() map[string]string {

	categories := map[string]string{
		"NEWS":          "NEWS",
		"ENTERTAINMENT": "ENTERTAINMENT",
		"POLITIK":       "POLITIK",
		"OTOMOTIF":      "OTOMOTIF",
	}
	return categories

}

//InitUserCategory to init in memory user category
func InitUserCategory() []domain.Category {
	categories := []domain.Category{
		domain.Category{
			Username:     "bambang",
			CategoryName: "MAKANAN",
			CategorySlug: "BAMBANG-MAKANAN",
		},
		domain.Category{
			Username:     "bambang",
			CategoryName: "MINUMAN",
			CategorySlug: "BAMBANG-MINUMAN",
		},
	}
	return categories
}
