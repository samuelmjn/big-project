package elastic

import (
	"context"
	"encoding/json"

	"github.com/olivere/elastic"
	"gitlab.com/samuelmjn/big-project/article/config"
	"gitlab.com/samuelmjn/big-project/article/domain"
)

func FindPublishedArticleElastic(ctx context.Context, keyword string) ([]domain.Article, error) {
	elasticClient := config.InitElastic()
	articles := []domain.Article{}
	article := domain.Article{}
	q := elastic.NewMultiMatchQuery(keyword, "name", "abbreviation").Type("phrase_prefix")
	result, err := elasticClient.Search().
		Index("article").
		Type("article").
		Query(q).
		Do(ctx)
	if err != nil {
		return []domain.Article{}, err
	}
	for _, hit := range result.Hits.Hits {
		err := json.Unmarshal(*hit.Source, &article)
		if err != nil {
			return []domain.Article{}, err
		}

		articles = append(articles, article)
	}

	return articles, nil
}
