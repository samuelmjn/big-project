package redis

import (
	"encoding/json"
	"errors"
	"log"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/samuelmjn/big-project/article/config"
	"gitlab.com/samuelmjn/big-project/article/domain"
)

func GetPublishedArticleBySlugCache(slug string) (domain.Article, error) {
	redisPool := config.InitRedis()
	conn := redisPool.Get()
	defer conn.Close()
	result, err := redis.String(conn.Do("GET", slug))
	log.Println("redisquery", err)
	if err == redis.ErrNil {
		return domain.Article{}, errors.New("not found")
	} else if err != nil {
		return domain.Article{}, err
	}

	article := domain.Article{}
	err = json.Unmarshal([]byte(result), &article)
	return article, nil
}
