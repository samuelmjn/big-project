package cockroach

import (
	"database/sql"

	"gitlab.com/samuelmjn/big-project/article/domain"
)

func parseRowsToArticle(rows *sql.Rows) []domain.Article {
	var result []domain.Article
	flag := make(map[string]bool)
	counter := 0
	for rows.Next() {
		var a domain.Article
		var c domain.CategoryV2
		rows.Scan(
			&a.ID, &a.Title, &a.Body, &a.Slug,
			&a.Status, &a.Thumbnail, &a.UserID, &a.CreatedAt, &a.UpdatedAt,
			&c.CategoryId, &c.CategoryName, &c.CategorySlug, &c.UserID,
		)
		if !flag[a.ID] {
			a.Categories = append(a.Categories, c)
			result = append(result, a)
			counter++
			flag[a.ID] = true
		} else {
			result[0].Categories = append(result[0].Categories, c)
		}
	}
	return result
}

func parseRowsToCategory(rows *sql.Rows) []domain.CategoryV2 {
	var result []domain.CategoryV2
	for rows.Next() {
		var c domain.CategoryV2
		rows.Scan(
			&c.CategoryId, &c.UserID, &c.CategoryName, &c.CategorySlug,
		)
		result = append(result, c)
	}
	return result
}
