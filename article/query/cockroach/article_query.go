package cockroach

import (
	"database/sql"
	"errors"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/samuelmjn/big-project/article/domain"

	"gitlab.com/samuelmjn/big-project/article/query"
)

type ArticleQueryCockroach struct {
	DB *sql.DB
}

func NewArticleQueryCockroach(DB *sql.DB) query.ArticleQuery {
	return &ArticleQueryCockroach{DB: DB}
}

func (q *ArticleQueryCockroach) GetPublishedArticlesByCategory(category string, limit int32, offset int32) query.QueryResult {
	articles := []domain.Article{}
	if category == "" {
		rows, err := q.DB.Query(`SELECT a.id, a.title, a.body, a.slug, a.status, a.thumbnail, a.userid, a.createdat, a.updatedat, c.id, c.categoryname, c.categoryslug, c.userid
		FROM article a
		JOIN article_categories ac ON ac.articleid = a.id
	 	JOIN category c ON c.id = ac.categoryid 
		WHERE a.status = 'PUBLISHED' ORDER BY a.updatedat DESC LIMIT $1 OFFSET $2 
		`, limit, offset)
		if err != nil {
			return query.QueryResult{Result: domain.Article{}, Error: err}
		}
		rowsResult := parseRowsToArticle(rows)
		articles = rowsResult
		return query.QueryResult{Result: articles, Error: nil}
	} else {
		rows, err := q.DB.Query(`SELECT a.id, a.title, a.body, a.slug, a.status, a.thumbnail, a.userid, a.createdat, a.updatedat, c.id, c.categoryname, c.categoryslug, c.userid
		FROM article a
		JOIN article_categories ac ON ac.articleid = a.id
		JOIN category c ON c.id = ac.categoryid
		WHERE a.status = 'PUBLISHED'  AND c.categoryslug = $1 ORDER BY a.updatedat DESC LIMIT $2 OFFSET $3
		`, category, limit, offset)
		if err != nil {
			return query.QueryResult{Result: domain.Article{}, Error: err}
		}
		rowsResult := parseRowsToArticle(rows)
		articles = rowsResult
		return query.QueryResult{Result: articles, Error: nil}
	}
}

func (q *ArticleQueryCockroach) GetArticleBySlug(slug string) query.QueryResult {
	articles := []domain.Article{}
	rows, err := q.DB.Query(`SELECT a.id, a.title, a.body, a.slug, a.status, a.thumbnail, a.userid, a.createdat, a.updatedat, c.id, c.categoryname, c.categoryslug, c.userid
					FROM article a
					LEFT JOIN article_categories ac ON ac.articleid = a.id
					LEFT JOIN category c ON c.id = ac.categoryid
					WHERE a.slug = $1
			`, slug)
	if err != nil {
		return query.QueryResult{Result: domain.Article{}, Error: err}
	}
	rowsResult := parseRowsToArticle(rows)
	articles = rowsResult
	if len(articles) == 0 {
		return query.QueryResult{Result: domain.Article{}, Error: errors.New("no article found")}
	}
	return query.QueryResult{Result: articles[0], Error: nil}
}

func (q *ArticleQueryCockroach) FindPublishedArticles(keyword string, offset int32, limit int32) query.QueryResult {
	articles := []domain.Article{}
	rows, err := q.DB.Query(`SELECT a.id, a.title, a.body, a.slug, a.status, a.thumbnail, a.userid, a.createdat, a.updatedat, c.id, c.categoryname, c.categoryslug, c.userid
					FROM article a
					LEFT JOIN article_categories ac ON ac.articleid = a.id
					LEFT JOIN category c ON c.id = ac.categoryid
					WHERE a.status = 'PUBLISHED' AND a.title ILIKE '%'||$1||'%' ORDER BY a.updatedat DESC offset $2 limit $3
			`, keyword, offset, limit)
	if err != nil {
		return query.QueryResult{Result: domain.Article{}, Error: err}
	}
	rowsResult := parseRowsToArticle(rows)
	articles = rowsResult
	return query.QueryResult{Result: articles, Error: nil}

}

func (q *ArticleQueryCockroach) ProfileArticles(userID string, category string, limit int32, offset int32) query.QueryResult {
	result := query.QueryResult{}
	articles := []domain.Article{}
	result.Result = articles
	if len(category) != 0 {
		if !q.CheckCategory(category) {
			result.Result = articles
			result.Error = errors.New("Category not valid")
			return result
		}
	}

	if category == "" {
		rows, err := q.DB.Query(`SELECT a.id, a.title, a.body, a.slug, a.status, a.thumbnail, a.userid, a.createdat, a.updatedat, c.id, c.categoryname, c.categoryslug, c.userid
					FROM article a
					LEFT JOIN article_categories ac ON ac.articleid = a.id
					LEFT JOIN category c ON c.id = ac.categoryid
					WHERE a.userid = $1 ORDER BY a.updatedat DESC offset $2 limit $3
			`, userID, offset, limit)
		if err != nil {
			return query.QueryResult{Result: domain.Article{}, Error: err}
		}
		rowsResult := parseRowsToArticle(rows)
		articles = rowsResult
		return query.QueryResult{Result: articles, Error: nil}
	}

	rows, err := q.DB.Query(`SELECT a.id, a.title, a.body, a.slug, a.status, a.thumbnail, a.userid, a.createdat, a.updatedat, c.id, c.categoryname, c.categoryslug, c.userid
					FROM article a
					LEFT JOIN article_categories ac ON ac.articleid = a.id
					LEFT JOIN category c ON c.id = ac.categoryid
					WHERE a.userid = $1 AND c.categoryslug = $2 ORDER BY a.updatedat DESC offset $3 limit $4
			`, userID, category, offset, limit)
	if err != nil {
		return query.QueryResult{Result: domain.Article{}, Error: err}
	}
	rowsResult := parseRowsToArticle(rows)
	articles = rowsResult
	return query.QueryResult{Result: articles, Error: nil}
}
func (q *ArticleQueryCockroach) GetCategoryWithUserid(userid, categoryname string) query.QueryResult {
	result := query.QueryResult{}
	categories := []domain.CategoryV2{}
	result.Result = categories
	rows, err := q.DB.Query(`SELECT * from category c where c.userid = $1 and categoryname = $2
			`, userid, categoryname)
	if err != nil {
		return query.QueryResult{Result: domain.CategoryV2{}, Error: err}
	}
	rowsResult := parseRowsToCategory(rows)
	categories = rowsResult
	return query.QueryResult{Result: categories, Error: nil}
}
func (q *ArticleQueryCockroach) GetCategoriesByUserid(userid string, limit int32, offset int32) query.QueryResult {
	result := query.QueryResult{}
	categories := []domain.CategoryV2{}
	result.Result = categories
	rows, err := q.DB.Query(`SELECT * from category c where c.userid = $1
			`, userid)
	if err != nil {
		return query.QueryResult{Result: domain.CategoryV2{}, Error: err}
	}
	rowsResult := parseRowsToCategory(rows)
	categories = rowsResult
	return query.QueryResult{Result: categories, Error: nil}
}

func (q *ArticleQueryCockroach) DashboardArticles(userID, keyword, category, status string, limit int32, offset int32) query.QueryResult {
	result := query.QueryResult{}
	articles := []domain.Article{}

	if status != "ALL" && status != "" && status != domain.Published && status != domain.Draft {
		result.Error = errors.New("Status not valid")

	} else {
		if status == "ALL" {
			status = ""
		}
		if category == "" || category == "ALL" {
			rows, err := q.DB.Query(`
					SELECT a.id, a.title, a.body, a.slug, a.status, a.thumbnail, a.userid, a.createdat, a.updatedat, c.id, c.categoryname, c.categoryslug, c.userid
					FROM article a
					LEFT JOIN article_categories ac ON ac.articleid = a.id
					LEFT JOIN category c ON c.id = ac.categoryid
					WHERE a.userid = $1  AND a.status LIKE '%'||$2||'%'  AND a.title ILIKE '%'||$3||'%' ORDER BY a.updatedat DESC offset $4 limit $5`, userID, status, keyword, offset, limit)

			if err != nil {
				return query.QueryResult{Result: domain.Article{}, Error: err}
			}
			rowsResult := parseRowsToArticle(rows)
			articles = rowsResult
		}

		rows, err := q.DB.Query(`
					SELECT a.id, a.title, a.body, a.slug, a.status, a.thumbnail, a.userid, a.createdat, a.updatedat, c.id, c.categoryname, c.categoryslug, c.userid
					FROM article a
					LEFT JOIN article_categories ac ON ac.articleid = a.id
					LEFT JOIN category c ON c.id = ac.categoryid
					WHERE a.userid = $1 AND c.categoryslug = $2  AND a.status LIKE '%'||$3||'%'  AND a.title ILIKE '%'||$4||'%' ORDER BY a.updatedat DESC offset $5 limit $6`, userID, category, status, keyword, offset, limit)

		if err != nil {
			return query.QueryResult{Result: domain.Article{}, Error: err}
		}
		rowsResult := parseRowsToArticle(rows)
		articles = rowsResult
	}
	return query.QueryResult{Result: articles, Error: nil}
}

func (q *ArticleQueryCockroach) CheckCategory(category string) bool {
	if category == domain.News || category == domain.Politik || category == domain.Otomotif || category == domain.Entertainment {
		return true
	} else {
		rows, err := q.DB.Query(`SELECT * FROM category c WHERE c.categoryslug = $1`, category)
		if err != nil {
			return false
		}
		for rows.Next() {
			categoryObj := domain.Category{}
			err = rows.Scan(&categoryObj.Username, &categoryObj.CategorySlug, &categoryObj.CategoryName)
			if err != nil {
				return false
			}
			if categoryObj.CategorySlug == category {

				return true
			}
		}
	}
	return false
}

func (q *ArticleQueryCockroach) GetArticleSlug(slug string) query.QueryResult {
	article := domain.Article{}
	err := q.DB.QueryRow(`SELECT * FROM article WHERE slug = $1`, slug).Scan(&article.Title, &article.Slug, &article.Body, &article.Status, &article.CreatedAt, &article.UpdatedAt, &article.Thumbnail, &article.UserID, &article.ID)
	log.Println(err)
	log.Println(article)
	if err != nil {
		return query.QueryResult{Result: article, Error: err}
	}
	return query.QueryResult{Result: article, Error: nil}
}

// func (q *ArticleQueryCockroach) GetArticlesByIds(ids []string) ([]domain.Article, error {
// 	articles := []domain.Article{}
// 	rows, err := q.DB.Query(`SELECT a.id, a.title, a.body, a.slug, a.status, a.thumbnail, a.userid, a.createdat, a.updatedat, c.id, c.categoryname, c.categoryslug, c.userid
// 		FROM article a
// 		JOIN article_categories ac ON ac.articleid = a.id
// 	 	JOIN category c ON c.id = ac.categoryid
// 		WHERE a.id = unnest($1)
// 		`, ids)
// 	if err != nil {
// 		return articles, err
// 	}
// 	rowsResult := parseRowsToArticle(rows)
// 	articles = rowsResult
// 	return articles,nil
// }
