package query

//QueryResult is a wrapper for result and error
type QueryResult struct {
	Result interface{}
	Error  error
}

//ArticleQuery is an interface for querying article
type ArticleQuery interface {
	GetPublishedArticlesByCategory(category string, limit int32, offset int32) QueryResult
	GetArticleBySlug(slug string) QueryResult
	GetArticleSlug(slug string) QueryResult
	FindPublishedArticles(keyword string, limit int32, offset int32) QueryResult
	ProfileArticles(userID string, category string, limit int32, offset int32) QueryResult
	DashboardArticles(userID, keyword, category, status string, limit int32, offset int32) QueryResult
	GetCategoryWithUserid(userid, categoryname string) QueryResult
	GetCategoriesByUserid(userid string, limit int32, offset int32) QueryResult
}
