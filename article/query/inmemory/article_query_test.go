package inmemory

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/samuelmjn/big-project/article/domain"
	"gitlab.com/samuelmjn/big-project/article/storage"
)

func TestCanGetPublishedArticles(t *testing.T) {
	//given
	storage := storage.CreateArticleStorage()
	articleQuery := NewArticleQueryInMemory(storage)

	//when
	result := articleQuery.GetPublishedArticles()

	//then
	assert.Nil(t, result.Error)
	assert.Equal(t, 2, len(result.Result.([]domain.Article)))
}

func TestCanGetPublishedArticlesByCategory(t *testing.T) {
	//given
	storage := storage.CreateArticleStorage()
	articleQuery := NewArticleQueryInMemory(storage)
	categoryInput := "NEWS"
	//when
	result := articleQuery.GetPublishedArticlesByCategory(categoryInput)

	//then
	assert.Nil(t, result.Error)
	assert.Equal(t, 1, len(result.Result.([]domain.Article)))
}

func TestCanGetPublishedArticleBySlug(t *testing.T) {
	//given
	storage := storage.CreateArticleStorage()
	articleQuery := NewArticleQueryInMemory(storage)
	slugInput := "berita-sample-1"
	//when
	result := articleQuery.GetPublishedArticleBySlug(slugInput)

	//then
	assert.Nil(t, result.Error)
	assert.NotNil(t, result.Result.(domain.Article))
}

func TestNoCategoryExists(t *testing.T) {
	//given
	storage := storage.CreateArticleStorage()
	articleQuery := NewArticleQueryInMemory(storage)
	slugInput := "berita-sample-1"
	//when
	result := articleQuery.GetPublishedArticleBySlug(slugInput)

	//then
	assert.Nil(t, result.Error)
	assert.NotNil(t, result.Result.(domain.Article))
}
