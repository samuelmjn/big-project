package inmemory

import (
	"errors"
	"sort"
	"strings"

	"gitlab.com/samuelmjn/big-project/article/query"

	"gitlab.com/samuelmjn/big-project/article/domain"
	"gitlab.com/samuelmjn/big-project/article/storage"
)

//ArticleQueryInMemory is an implement from article query
type ArticleQueryInMemory struct {
	Storage *storage.ArticleStorage
}

//NewArticleQueryInMemory is for injecting storage into query
func NewArticleQueryInMemory(storage *storage.ArticleStorage) *ArticleQueryInMemory {
	return &ArticleQueryInMemory{
		Storage: storage,
	}
}

//GetPublishedArticles is to return data from storage
func (articleQuery *ArticleQueryInMemory) GetPublishedArticles() query.QueryResult {
	result := query.QueryResult{}
	var articles []domain.Article

	for _, v := range articleQuery.Storage.ArticleMap {
		if v.Status == "PUBLISHED" {
			articles = append(articles, v)
		}
	}

	articles = SortByDate(articles)

	result.Result = articles
	result.Error = nil
	return result
}

//GetPublishedArticlesByCategory is to return data from storage
func (articleQuery *ArticleQueryInMemory) GetPublishedArticlesByCategory(category string, offset int32, limit int32) query.QueryResult {
	result := query.QueryResult{}
	var articles []domain.Article

	if len(category) != 0 {
		if !articleQuery.CheckCategory(category) {
			result.Error = errors.New("Category not valid")
			return result
		}
	}
	for _, v := range articleQuery.Storage.ArticleMap {
		if strings.Contains(v.Categories, category) && v.Status == domain.Published {
			articles = append(articles, v)
		}
	}
	articles = SortByDate(articles)

	result.Result = articles
	result.Error = nil
	return result
}

//GetPublishedArticleBySlug is to return data from storage
func (articleQuery *ArticleQueryInMemory) GetPublishedArticleBySlug(slug string) query.QueryResult {
	result := query.QueryResult{}

	for _, v := range articleQuery.Storage.ArticleMap {
		if v.Slug == slug {
			result.Result = v
			result.Error = nil
			return result
		}
	}

	result.Result = domain.Article{}
	result.Error = errors.New("Article not found")
	return result
}

//FindPublishedArticles is return articles
func (articleQuery *ArticleQueryInMemory) FindPublishedArticles(keyword string) query.QueryResult {
	result := query.QueryResult{}
	var articles []domain.Article
	for _, v := range articleQuery.Storage.ArticleMap {
		title := strings.ToLower(v.Title)
		if strings.Contains(title, keyword) && v.Status == "PUBLISHED" {
			articles = append(articles, v)
		}
	}
	articles = SortByDate(articles)

	result.Result = articles
	result.Error = nil
	return result

}

func SortByDate(articles []domain.Article) []domain.Article {
	sort.Slice(articles, func(i, j int) bool { return articles[i].CreatedAt.After(articles[j].CreatedAt) })
	return articles
}

func (articleQuery *ArticleQueryInMemory) CheckCategory(category string) bool {
	_, exist := articleQuery.Storage.CategoryMap[category]
	for _, v := range articleQuery.Storage.UserCategoryMap {
		if v.CategorySlug == category || exist {
			return true
		}
	}
	return false
}

func (articleQuery *ArticleQueryInMemory) GetPublishedArticlesByUserID(userID string) query.QueryResult {
	result := query.QueryResult{}
	var articles []domain.Article
	for _, v := range articleQuery.Storage.ArticleMap {
		if v.UserID == userID {
			articles = append(articles, v)
		}
	}
	articles = SortByDate(articles)

	result.Result = articles
	result.Error = nil
	return result

}

func (articleQuery *ArticleQueryInMemory) GetArticleSlug(slug string) query.QueryResult {
	result := query.QueryResult{}
	for _, v := range articleQuery.Storage.ArticleMap {
		if v.Slug == slug {
			result.Result = v
			result.Error = nil
			return result
		}
	}
	result.Result = nil
	result.Error = errors.New("Article slug not found")
	return result
}

func (articleQuery *ArticleQueryInMemory) GetArticlesByUserID(userID, status string) query.QueryResult {
	result := query.QueryResult{}
	var articles []domain.Article

	for _, v := range articleQuery.Storage.ArticleMap {
		if status == "ALL" || status == "" {
			if v.UserID == userID {
				articles = append(articles, v)
			}
		} else {
			if v.UserID == userID && v.Status == status {
				articles = append(articles, v)
			}
		}
	}

	articles = SortByDate(articles)

	result.Result = articles
	result.Error = nil
	return result
}

func (articleQuery *ArticleQueryInMemory) FindArticlesByUserID(keyword, userID, status string) query.QueryResult {
	result := query.QueryResult{}
	var articles []domain.Article
	for _, v := range articleQuery.Storage.ArticleMap {
		title := strings.ToLower(v.Title)
		if status == "ALL" || status == "" {
			if strings.Contains(title, keyword) && v.UserID == userID {
				articles = append(articles, v)
			}
		} else {
			if strings.Contains(title, keyword) && v.UserID == userID && v.Status == status {
				articles = append(articles, v)
			}
		}
	}
	articles = SortByDate(articles)

	result.Result = articles
	result.Error = nil
	return result

}

func (articleQuery *ArticleQueryInMemory) GetCategoryWithUsername(username, categoryname string) query.QueryResult {
	result := query.QueryResult{}
	userCategory := articleQuery.Storage.UserCategoryMap
	for _, v := range userCategory {
		if v.CategoryName == categoryname && v.Username == username {
			result.Result = true
			return result
		}
	}
	result.Result = false
	return result
}

func (articleQuery *ArticleQueryInMemory) GetCategoriesByUsername(username string) query.QueryResult {
	result := query.QueryResult{}
	userCategory := articleQuery.Storage.UserCategoryMap
	var categories []domain.Category
	for _, v := range userCategory {
		if v.Username == username {
			categories = append(categories, v)
		}
	}

	result.Result = categories
	result.Error = nil
	return result
}

func (articleQuery *ArticleQueryInMemory) ProfileArticles(userID string, category string) query.QueryResult {
	result := query.QueryResult{}
	var articles []domain.Article

	if len(category) != 0 {
		if !articleQuery.CheckCategory(category) {
			result.Error = errors.New("Category not valid")
			return result
		}
	}
	for _, v := range articleQuery.Storage.ArticleMap {
		if strings.Contains(v.Categories, category) && v.Status == domain.Published && v.UserID == userID {
			articles = append(articles, v)
		}
	}

	articles = SortByDate(articles)

	result.Result = articles
	result.Error = nil
	return result
}

func (articleQuery *ArticleQueryInMemory) DashboardArticles(userID, keyword, category, status string) query.QueryResult {
	result := query.QueryResult{}
	var articles []domain.Article

	if len(category) != 0 {
		if !articleQuery.CheckCategory(category) {
			result.Error = errors.New("Category not valid")
			return result
		}
	}
	if (status != "ALL") && (status != "") && (status != domain.Published) && (status != domain.Draft) {
		result.Result = articles
		result.Error = errors.New("Status not valid")
		return result
	}

	for _, v := range articleQuery.Storage.ArticleMap {
		if status == "ALL" || status == "" {
			if strings.Contains(v.Categories, category) && v.UserID == userID && strings.Contains(v.Title, keyword) {
				articles = append(articles, v)
			}
		} else {
			if strings.Contains(v.Categories, category) && v.Status == status && v.UserID == userID && strings.Contains(v.Title, keyword) {
				articles = append(articles, v)
			}
		}
	}

	articles = SortByDate(articles)

	result.Result = articles
	result.Error = nil
	return result
}
