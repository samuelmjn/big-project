package server

import (
	"context"
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	"gitlab.com/samuelmjn/big-project/article/domain"

	"gitlab.com/samuelmjn/big-project/article/proto"
)

type ArticleServerService interface {
	FindPublishedArticles(keyword string, offset int32, limit int32) ([]domain.Article, error)
	GetPublishedArticlesByCategory(category string, offset int32, limit int32) ([]domain.Article, error)
	GetArticleBySlug(slug string) (domain.Article, error)
	CreateArticle(title, slug, body, thumbnail, userId, status string, categories []string) (domain.Article, error)
	CreateUserCategory(userid, categoryname, categoryslug string) (domain.CategoryV2, error)
	GetUserCategoriesByUserid(userid string, offset int32, limit int32) ([]domain.CategoryV2, error)
	UpdateArticle(articleID, userID, title, slug, body, thumbnail, status string, categories []string) (domain.Article, error)
	ProfileArticles(userID string, category string, offset int32, limit int32) ([]domain.Article, error)
	DashboardArticles(userID, keyword, category, status string, offset int32, limit int32) ([]domain.Article, error)
}

type ArticleServer struct {
	ArticleService ArticleServerService
}

func NewArticleServer(services ArticleServerService) proto.ArticleServiceServer {
	return &ArticleServer{
		ArticleService: services,
	}
}

func (s *ArticleServer) GetPublishedArticlesByCategory(ctx context.Context, in *proto.ArticleGetByCategoryRequest) (*proto.ArticleListResponse, error) {
	result := proto.ArticleListResponse{}
	category := in.Category
	offset := in.Offset
	limit := in.Limit
	articles, err := s.ArticleService.GetPublishedArticlesByCategory(category, offset, limit)
	if err != nil {
		return &result, grpc.Errorf(codes.NotFound, err.Error())
	}
	fmt.Println(articles)

	articlesProto := make([]*proto.Article, len(articles))
	for k, v := range articles {
		article, _ := ParseArticleToProto(v)
		articlesProto[k] = &article
	}

	result.Article = articlesProto
	return &result, nil

}

func (s *ArticleServer) GetArticleBySlug(ctx context.Context, in *proto.ArticleGetBySlugRequest) (*proto.Article, error) {
	result := proto.Article{}
	slug := in.Slug
	article, err := s.ArticleService.GetArticleBySlug(slug)
	if err != nil {
		return &result, grpc.Errorf(codes.NotFound, err.Error())
	}
	articleProto, _ := ParseArticleToProto(article)
	result = articleProto
	return &result, nil
}

func (s *ArticleServer) CreateArticle(ctx context.Context, in *proto.CreateArticleRequest) (*proto.Article, error) {

	result := proto.Article{}
	article, err := s.ArticleService.CreateArticle(in.Title, in.Slug, in.Body, in.Thumbnail, in.UserId, in.Status, in.Categories)
	if err != nil {
		return &result, grpc.Errorf(codes.FailedPrecondition, err.Error())
	}
	articleProto, _ := ParseArticleToProto(article)
	result = articleProto
	return &result, nil
}

func (s *ArticleServer) FindPublishedArticles(ctx context.Context, in *proto.SearchInput) (*proto.ArticleListResponse, error) {
	result := proto.ArticleListResponse{}
	keyword := in.Keyword
	offset := in.Offset
	limit := in.Limit
	articles, err := s.ArticleService.FindPublishedArticles(keyword, offset, limit)
	if err != nil {
		return &result, grpc.Errorf(codes.NotFound, err.Error())
	}

	articlesProto := make([]*proto.Article, len(articles))
	for k, v := range articles {
		article, _ := ParseArticleToProto(v)
		articlesProto[k] = &article
	}

	result.Article = articlesProto
	return &result, nil
}

func (s *ArticleServer) CreateUserCategory(ctx context.Context, in *proto.CreateUserCategoryInput) (*proto.UserCategory, error) {
	result := proto.UserCategory{}
	userId := in.Userid
	categoryname := in.Categoryname
	categoryslug := in.Categoryslug
	category, err := s.ArticleService.CreateUserCategory(userId, categoryname, categoryslug)
	if err != nil {
		return &result, grpc.Errorf(codes.NotFound, err.Error())
	}
	result = proto.UserCategory{
		Userid:       category.UserID,
		Categoryid:   category.CategoryId,
		Categoryname: category.CategoryName,
		Categoryslug: category.CategorySlug,
	}
	return &result, nil
}

func (s *ArticleServer) GetUserCategoriesByUserid(ctx context.Context, in *proto.GetUserCategoryInput) (*proto.UserCategories, error) {
	result := proto.UserCategories{}
	userid := in.Userid
	offset := in.Offset
	limit := in.Limit
	categories, err := s.ArticleService.GetUserCategoriesByUserid(userid, offset, limit)
	if err != nil {
		return &result, err
	}
	userCategoriesProto := make([]*proto.UserCategory, len(categories))
	for k, v := range categories {
		userCategory := proto.UserCategory{
			Userid:       v.UserID,
			Categoryid:   v.CategoryId,
			Categoryname: v.CategoryName,
			Categoryslug: v.CategorySlug,
		}
		userCategoriesProto[k] = &userCategory
	}

	result.UserCategory = userCategoriesProto
	return &result, nil

}

func (s *ArticleServer) UpdateArticle(ctx context.Context, in *proto.CreateArticleRequest) (*proto.Article, error) {
	result := proto.Article{}
	article, err := s.ArticleService.UpdateArticle(in.Id, in.UserId, in.Title, in.Slug, in.Body, in.Thumbnail, in.Status, in.Categories)
	if err != nil {
		return &result, grpc.Errorf(codes.FailedPrecondition, err.Error())
	}
	articleProto, _ := ParseArticleToProto(article)
	result = articleProto
	return &result, nil
}

func (s *ArticleServer) ProfileArticles(ctx context.Context, in *proto.ProfileArticlesRequest) (*proto.ArticleListResponse, error) {
	result := proto.ArticleListResponse{}
	userid := in.Userid
	category := in.Category
	offset := in.Offset
	limit := in.Limit
	articles, err := s.ArticleService.ProfileArticles(userid, category, offset, limit)
	if err != nil {
		return &result, grpc.Errorf(codes.NotFound, err.Error())
	}

	articlesProto := make([]*proto.Article, len(articles))
	for k, v := range articles {
		article, _ := ParseArticleToProto(v)
		articlesProto[k] = &article
	}

	result.Article = articlesProto
	return &result, nil

}

func (s *ArticleServer) DashboardArticles(ctx context.Context, in *proto.DashboardArticlesRequest) (*proto.ArticleListResponse, error) {
	result := proto.ArticleListResponse{}
	userid := in.Userid
	keyword := in.Keyword
	category := in.Category
	status := in.Status
	offset := in.Offset
	limit := in.Limit
	articles, err := s.ArticleService.DashboardArticles(userid, keyword, category, status, offset, limit)
	if err != nil {
		return &result, grpc.Errorf(codes.NotFound, err.Error())
	}

	articlesProto := make([]*proto.Article, len(articles))
	for k, v := range articles {
		article, _ := ParseArticleToProto(v)
		articlesProto[k] = &article
	}

	result.Article = articlesProto
	return &result, nil

}
