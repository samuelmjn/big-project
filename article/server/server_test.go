package server

import (
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/samuelmjn/big-project/article/domain"
)

type MockArticleService struct {
	mock.Mock
}

func (mock MockArticleService) CreateArticle(name, body string) (domain.Article, error) {
	args := mock.Called(name, body)
	return args.Get(0).(domain.Article), args.Error(1)
}

func (mock MockArticleService) GetPublishedArticles() ([]domain.Article, error) {
	args := mock.Called()
	return args.Get(0).([]domain.Article), args.Error(1)
}

func (mock MockArticleService) CreateCategory(name, body string) (domain.Article, error) {
	args := mock.Called(name, body)
	return args.Get(0).(domain.Article), args.Error(1)
}

func (mock MockArticleService) GetPublishedArticlesByCategory(category string) ([]domain.Article, error) {
	args := mock.Called(category)
	return args.Get(0).([]domain.Article), args.Error(0)
}
func TestCanGetPublishedArticlesByCategory(t *testing.T) {
	//given
	
	//when

	//then
}
