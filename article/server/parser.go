package server

import (
	"github.com/golang/protobuf/ptypes"
	"gitlab.com/samuelmjn/big-project/article/domain"
	"gitlab.com/samuelmjn/big-project/article/proto"
)

//ParseArticleToProto parsing article to proto article
func ParseArticleToProto(article domain.Article) (proto.Article, error) {
	createdAt, err := ptypes.TimestampProto(article.CreatedAt)
	if err != nil {
		return proto.Article{}, err
	}

	updatedAt, err := ptypes.TimestampProto(article.UpdatedAt)
	if err != nil {
		return proto.Article{}, err
	}
	var categories []*proto.UserCategory
	for _, v := range article.Categories {
		cat := &proto.UserCategory{
			Userid:       v.UserID,
			Categoryid:   v.CategoryId,
			Categoryname: v.CategoryName,
			Categoryslug: v.CategorySlug,
		}
		categories = append(categories, cat)
	}
	return proto.Article{
		Id:         article.ID,
		Title:      article.Title,
		Slug:       article.Slug,
		Body:       article.Body,
		Status:     article.Status,
		CreatedAt:  createdAt,
		UpdatedAt:  updatedAt,
		Categories: categories,
		UserId:     article.UserID,
		Thumbnail:  article.Thumbnail,
	}, nil
}
