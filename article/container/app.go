package container

import (
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/samuelmjn/big-project/article/config"
	"gitlab.com/samuelmjn/big-project/article/query"
	"gitlab.com/samuelmjn/big-project/article/repository"

	cq "gitlab.com/samuelmjn/big-project/article/query/cockroach"
	cr "gitlab.com/samuelmjn/big-project/article/repository/cockroach"
)

type Article struct {
	Query      query.ArticleQuery
	Repository repository.ArticleRepository
}

type App struct {
	Article Article
}

var (
	driver = "cockroach"
)

func InitApp() *App {
	LoadEnv()
	switch driver {
	case "cockroach":
		db := config.InitCockroach()

		return &App{
			Article: Article{
				Query:      cq.NewArticleQueryCockroach(db),
				Repository: cr.NewArticleRepositoryCockroach(db),
			},
		}
	default:
		// db := storage.CreateArticleStorage()
		return &App{
			// Article: Article{
			// 	Query:      iq.NewArticleQueryInMemory(db),
			// 	Repository: ir.NewArticleRepositoryInMemory(db),
			// },
		}
	}

}

func LoadEnv() {
	godotenv.Load()

	// Check the environment variable
	env := os.Getenv("ENV")
	if env != "production" {
		driver = "cockroach"

	} else {
		driver = ""
	}
}
