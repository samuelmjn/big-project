package domain

import (
	"crypto/rand"
	"errors"
	"fmt"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
)

const (
	//Published status
	Published string = "PUBLISHED"
	//Draft status
	Draft string = "DRAFT"
)

const (
	News          string = "NEWS"
	Politik       string = "POLITIK"
	Otomotif      string = "OTOMOTIF"
	Entertainment string = "ENTERTAINMENT"
)

type Article struct {
	ID         string
	Title      string
	Slug       string
	Body       string
	Status     string
	CreatedAt  time.Time
	UpdatedAt  time.Time
	Categories []CategoryV2
	Thumbnail  string
	UserID     string
}

type ArticleService interface {
	CheckSlugExists(slug string) bool
}

func CreateArticle(as ArticleService, title, slug, body, thumbnail, userId string, status string, categories ...string) (Article, error) {
	if len(title) < 10 && len(title) > 125 {
		return Article{}, errors.New("Title lenght must between 10 and 125")
	}

	if len(slug) == 0 {
		slug = GenerateArticleSlug(title)
	} else {
		slugExists := as.CheckSlugExists(slug)
		if slugExists {
			return Article{}, errors.New("Please change slug, Slug already exists")
		}
	}

	return Article{
		ID:        uuid.NewV4().String(),
		Title:     title,
		Slug:      slug,
		Body:      body,
		Status:    status,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		Thumbnail: thumbnail,
		UserID:    userId,
	}, nil

}

func EditArticle(as ArticleService, title, slug, body, thumbnail, userId string, status string, categories ...string) (Article, error) {

	if len(title) < 10 && len(title) > 125 {
		return Article{}, errors.New("Title lenght must between 10 and 125")
	}

	return Article{
		Title:     title,
		Slug:      slug,
		Body:      body,
		Status:    status,
		UpdatedAt: time.Now(),
		Thumbnail: thumbnail,
		UserID:    userId,
	}, nil

}

//GenerateArticleSlug is to generate uniq slug
func GenerateArticleSlug(title string) string {
	randomID := make([]byte, 4)
	rand.Read(randomID)
	randomIDString := fmt.Sprintf("%x", randomID)

	slug := strings.TrimSpace(title)
	slug = strings.ReplaceAll(slug, " ", "-")
	slug += "-" + randomIDString

	return strings.ToLower(slug)

}
