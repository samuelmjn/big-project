package domain

import (
	"testing"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockArticleService struct {
	mock.Mock
}

func (s MockArticleService) CheckSlugExists(slug string) bool {
	args := s.Called(slug)
	return args.Bool(0)
}

func TestCanCreatePublishedArticle(t *testing.T) {
	//given
	title := "Berita Kriminal"
	slug := "berita-kriminal"
	body := "Body berita kriminal"
	status := Published
	thumbnail := "thumbnail"
	categories := []string{
		News,
		Politik,
	}
	userId := uuid.NewV4().String()
	as := MockArticleService{}
	as.On("CheckSlugExists", slug).Return(false)

	//when

	article, err := CreateArticle(as, title, slug, body, thumbnail, userId, status, categories...)

	//then
	assert.Nil(t, err)
	assert.Equal(t, Published, article.Status)
	assert.NotNil(t, article)
}
func TestCantCreatePublishedArticle(t *testing.T) {
	//given
	title := "Berita Kriminal"
	slug := "berita-kriminal"
	body := "Body berita kriminal"
	status := Published
	thumbnail := "thumbnail"
	categories := []string{
		News,
		Politik,
	}
	userId := uuid.NewV4().String()
	as := MockArticleService{}
	as.On("CheckSlugExists", slug).Return(true)

	//when

	_, err := CreateArticle(as, title, slug, body, thumbnail, userId, status, categories...)

	//then
	assert.NotNil(t, err)
}

func TestCanCreateDraftArticle(t *testing.T) {
	//given
	title := "Berita Kriminal"
	slug := "berita-kriminal"
	body := "Body berita kriminal"
	status := Draft
	thumbnail := "thumbnail"
	categories := []string{
		News,
		Politik,
	}
	userId := uuid.NewV4().String()
	as := MockArticleService{}
	as.On("CheckSlugExists", slug).Return(false)

	//when
	article, err := CreateArticle(as, title, slug, body, thumbnail, userId, status, categories...)

	//then
	assert.Nil(t, err)
	assert.Equal(t, Draft, article.Status)
	assert.NotNil(t, article)
}
