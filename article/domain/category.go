package domain

import (
	"errors"

	uuid "github.com/satori/go.uuid"
)

type Category struct {
	Username     string
	CategorySlug string
	CategoryName string
}

type CategoryV2 struct {
	CategoryId   string
	UserID       string
	CategorySlug string
	CategoryName string
}

type CategoryService interface {
	CheckUserHasCategoryExist(username, categoryname string) bool
}

func CreateCategory(service CategoryService, username string, categoryName string, categorySlug string) (Category, error) {
	categoryExists := service.CheckUserHasCategoryExist(username, categoryName)
	if categoryExists {
		return Category{}, errors.New("Category already exists")
	}
	if len(categorySlug) == 0 {
		categorySlug = username + "-" + categoryName
	}
	return Category{
		Username:     username,
		CategorySlug: categorySlug,
		CategoryName: categoryName,
	}, nil
}

func CreateCategoryV2(service CategoryService, userid string, categoryName string, categorySlug string) (CategoryV2, error) {
	return CategoryV2{
		CategoryId:   uuid.NewV4().String(),
		UserID:       userid,
		CategorySlug: categorySlug,
		CategoryName: categoryName,
	}, nil
}
