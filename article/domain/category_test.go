package domain

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type MockCategoryService struct {
	mock.Mock
}

func (s MockCategoryService) CheckUserHasCategoryExist(username, categoryname string) bool {
	args := s.Called(username, categoryname)
	return args.Bool(0)
}

func TestCanCreateCategory(t *testing.T) {
	//given
	username := "bambang"
	categoryName := "MAKANAN"
	categorySlug := "BAMBANG-MAKANAN"

	cs := MockCategoryService{}
	cs.On("CheckUserHasCategoryExist", username, categoryName).Return(false)

	category, err := CreateCategory(cs, username, categoryName, categorySlug)

	assert.Nil(t, err)
	assert.NotEmpty(t, category)

}
