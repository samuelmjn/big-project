package redis

import (
	"encoding/json"

	"gitlab.com/samuelmjn/big-project/article/config"
	"gitlab.com/samuelmjn/big-project/article/domain"
)

func CreateArticleCache(article domain.Article) error {
	redisPool := config.InitRedis()
	conn := redisPool.Get()
	defer conn.Close()

	json, err := json.Marshal(article)
	if err != nil {
		return err
	}

	_, err = conn.Do("SET", article.Slug, json)
	if err != nil {
		return err
	}

	return nil

}
