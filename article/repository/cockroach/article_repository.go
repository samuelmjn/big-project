package cockroach

import (
	"database/sql"
	"fmt"

	"gitlab.com/samuelmjn/big-project/article/domain"
	"gitlab.com/samuelmjn/big-project/article/repository"
)

type ArticleRepositoryCockroach struct {
	DB *sql.DB
}

func NewArticleRepositoryCockroach(DB *sql.DB) repository.ArticleRepository {
	return &ArticleRepositoryCockroach{DB: DB}
}

func (r *ArticleRepositoryCockroach) SaveArticle(article *domain.Article) error {
	fmt.Println(article.Thumbnail)
	_, err := r.DB.Exec(`INSERT INTO article (title, slug, body, status, createdat, updatedat, thumbnail, userid, id) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)`,
		article.Title,
		article.Slug,
		article.Body,
		article.Status,
		article.CreatedAt,
		article.UpdatedAt,
		article.Thumbnail,
		article.UserID,
		article.ID)
	if err != nil {
		return err
	}
	return nil
}

func (r *ArticleRepositoryCockroach) SaveCategory(category *domain.Category) error {
	return nil
}

func (r *ArticleRepositoryCockroach) SaveCategoryV2(category *domain.CategoryV2) error {
	_, err := r.DB.Exec(`INSERT INTO category(id, userid, categoryslug, categoryname) VALUES ($1,$2,$3,$4)`,
		category.CategoryId,
		category.UserID,
		category.CategorySlug,
		category.CategoryName)
	if err != nil {
		return err
	}
	return nil
}

func (r *ArticleRepositoryCockroach) SaveArticleCategories(articleId string, categoryId string) error {
	_, err := r.DB.Exec(`INSERT INTO article_categories(articleid, categoryid) VALUES ($1,$2)`,
		articleId,
		categoryId)
	if err != nil {
		return err
	}
	return nil
}

func (r *ArticleRepositoryCockroach) UpdateArticle(articleID string, article domain.Article) error {
	_, err := r.DB.Exec(`UPDATE article SET title = $1, slug = $2, body = $3, status = $4, updatedat = $5, thumbnail = $6 WHERE id = $7`, article.Title, article.Slug, article.Body, article.Status, article.UpdatedAt, article.Thumbnail, articleID)
	if err != nil {
		return err
	}
	return nil
}

func (r *ArticleRepositoryCockroach) UpdateArticleCategories(articleID string, categoryIds []string) error {
	_, err := r.DB.Exec(`
	INSERT INTO article_categories(articleid, categoryid) 
	SELECT $2, x
	FROM    unnest($1) x
	`, categoryIds, articleID)
	if err != nil {
		return err
	}
	return nil
}
