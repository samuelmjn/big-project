package inmemory

import (
	"fmt"
	"log"

	"gitlab.com/samuelmjn/big-project/article/domain"
	"gitlab.com/samuelmjn/big-project/article/storage"
)

//ArticleRepositoryInMemory is wrapper for storage
type ArticleRepositoryInMemory struct {
	Storage *storage.ArticleStorage
}

//NewArticleQueryInMemory is for injecting storage into query
func NewArticleRepositoryInMemory(storage *storage.ArticleStorage) *ArticleRepositoryInMemory {
	return &ArticleRepositoryInMemory{
		Storage: storage,
	}
}

//SaveArticle is to save article into storage
func (repository *ArticleRepositoryInMemory) SaveArticle(article *domain.Article) error {
	fmt.Println(repository.Storage.ArticleMap)
	repository.Storage.ArticleMap = append(repository.Storage.ArticleMap, *article)
	fmt.Println(repository.Storage.ArticleMap)
	return nil
}

func (repository *ArticleRepositoryInMemory) SaveCategory(category *domain.Category) error {
	fmt.Println(repository.Storage.ArticleMap)
	repository.Storage.UserCategoryMap = append(repository.Storage.UserCategoryMap, *category)
	fmt.Println(repository.Storage.ArticleMap)
	return nil
}

func (repository *ArticleRepositoryInMemory) UpdateArticle(articleID string, article domain.Article) error {
	new := article
	for k, v := range repository.Storage.ArticleMap {
		if articleID == v.ID {
			repository.Storage.ArticleMap[k] = new
		}
	}

	log.Println(new)
	return nil
}
