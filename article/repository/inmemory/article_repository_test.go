package inmemory

import (
	"testing"
	"time"

	"gitlab.com/samuelmjn/big-project/article/storage"

	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/samuelmjn/big-project/article/domain"
)

func TestCanSaveArticle(t *testing.T) {
	//given
	articleTest := domain.Article{
		ID:         uuid.NewV4().String(),
		Title:      "Berita Sample",
		Slug:       "berita-sample-2",
		Body:       "body berita sample",
		Status:     "PUBLISHED",
		CreatedAt:  time.Now(),
		UpdatedAt:  time.Now(),
		Categories: "NEWS;ENTERTAINMENT",
		Thumbnail:  "https://image-store.slidesharecdn.com/084231bb-98d6-476e-ac6f-06da2abf8cfe-original.jpeg",
		UserID:     "aaaaa",
	}
	//when
	storage := storage.CreateArticleStorage()
	repository := NewArticleRepositoryInMemory(storage)
	err := repository.SaveArticle(&articleTest)

	//then
	assert.Nil(t, err)

}
