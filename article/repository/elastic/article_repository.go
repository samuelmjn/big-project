package elastic

import (
	"context"

	"gitlab.com/samuelmjn/big-project/article/config"
	"gitlab.com/samuelmjn/big-project/article/domain"
)

func CreateArticle(ctx context.Context, article domain.Article) error {
	elasticClient := config.InitElastic()
	_, err := elasticClient.Index().
		Index("article").
		Type("article").
		Id(article.ID).
		BodyJson(article).
		Do(ctx)
	if err != nil {
		return err
	}

	return nil
}
