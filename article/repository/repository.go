package repository

import (
	"gitlab.com/samuelmjn/big-project/article/domain"
)

type ArticleRepository interface {
	SaveArticle(article *domain.Article) error
	SaveCategory(category *domain.Category) error
	SaveCategoryV2(category *domain.CategoryV2) error
	SaveArticleCategories(articleId, categoryid string) error
	UpdateArticle(articleID string, article domain.Article) error
	UpdateArticleCategories(articleID string, categoryIds []string) error
}
