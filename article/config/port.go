package config

import (
	"os"

	"github.com/joho/godotenv"
)

var (
	// ENV store environment variable
	ENV string

	// GRPCArticleServerPort port for serving article service
	GRPCServerPort = "article:9200"
)

//InitPort to load env for port
func InitPort() {
	godotenv.Load()

	// Check the environment variable
	env := os.Getenv("ENV")
	if env != "production" {
		ENV = "development"
		GRPCServerPort = ":9200"

	} else {
		GRPCServerPort = "article:9200"
		ENV = "production"
	}
}
