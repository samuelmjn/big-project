package main

import (
	"log"
	"net"

	"gitlab.com/samuelmjn/big-project/article/config"
	"gitlab.com/samuelmjn/big-project/article/container"

	"gitlab.com/samuelmjn/big-project/article/proto"
	"gitlab.com/samuelmjn/big-project/article/server"
	"gitlab.com/samuelmjn/big-project/article/service"
	"google.golang.org/grpc"
)

func main() {
	config.InitPort()
	srv := grpc.NewServer()
	app := container.InitApp()
	service := service.NewArticleService(app)
	articleServer := server.NewArticleServer(service)
	proto.RegisterArticleServiceServer(srv, articleServer)
	log.Println("Starting gRPC Server at", config.GRPCServerPort)
	listen, err := net.Listen("tcp", config.GRPCServerPort)
	if err != nil {
		log.Fatalf("could not listen to %s: %v", config.GRPCServerPort, err)
	}
	log.Fatal("Server is listening", srv.Serve(listen))
}
