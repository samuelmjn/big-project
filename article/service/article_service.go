package service

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/samuelmjn/big-project/article/container"
	"gitlab.com/samuelmjn/big-project/article/domain"
	"gitlab.com/samuelmjn/big-project/article/query"

	esq "gitlab.com/samuelmjn/big-project/article/query/elastic"
	caq "gitlab.com/samuelmjn/big-project/article/query/redis"
	"gitlab.com/samuelmjn/big-project/article/repository"
	esr "gitlab.com/samuelmjn/big-project/article/repository/elastic"
	car "gitlab.com/samuelmjn/big-project/article/repository/redis"
)

//ArticleService is to wrap query and repo and gateway for grpc server
type ArticleService struct {
	Query      query.ArticleQuery
	Repository repository.ArticleRepository
}

//NewArticleService injector of article service
func NewArticleService(app *container.App) *ArticleService {
	return &ArticleService{
		Query:      app.Article.Query,
		Repository: app.Article.Repository,
	}
}

//FindPublishedArticles is to return articles based onn keyword
func (service *ArticleService) FindPublishedArticles(keyword string, offset int32, limit int32) ([]domain.Article, error) {
	articles := []domain.Article{}
	esResult, err := esq.FindPublishedArticleElastic(context.Background(), keyword)
	if err != nil {
		result := service.Query.FindPublishedArticles(keyword, offset, limit)
		if result.Error != nil {
			return nil, result.Error
		}
		articleDB := ConvertResultToArticleArray(result)
		articles = articleDB
	} else {
		articles = esResult
	}

	return articles, nil
}

//GetPublishedArticlesByCategory is to return articles based on category
func (service *ArticleService) GetPublishedArticlesByCategory(category string, offset int32, limit int32) ([]domain.Article, error) {
	result := service.Query.GetPublishedArticlesByCategory(category, limit, offset)
	if result.Error != nil {
		return nil, result.Error
	}
	articles := ConvertResultToArticleArray(result)
	return articles, nil
}

//GetPublishedArticleBySlug is to return articles based on category
func (service *ArticleService) GetArticleBySlug(slug string) (domain.Article, error) {
	article2 := domain.Article{}
	article, err := caq.GetPublishedArticleBySlugCache(slug)
	if err != nil {
		log.Println("ini dari db")
		result := service.Query.GetArticleBySlug(slug)
		if result.Error != nil {
			return domain.Article{}, result.Error
		} else {
			art := result.Result.(domain.Article)
			article2 = art
			_ = car.CreateArticleCache(art)
		}
	} else {
		log.Println("ini dari redis")
		article2 = article
	}
	return article2, nil
}

func (service *ArticleService) CreateArticle(title, slug, body, thumbnail, userId, status string, categoriesId []string) (domain.Article, error) {
	var categoriesInput []string
	article, err := domain.CreateArticle(service, title, slug, body, thumbnail, userId, status, categoriesInput...)
	log.Println(article)
	if err != nil {
		return domain.Article{}, err
	}
	errSave := service.Repository.SaveArticle(&article)
	if errSave != nil {
		return domain.Article{}, err
	}
	for _, categoryId := range categoriesId {
		service.Repository.SaveArticleCategories(article.ID, categoryId)
	}

	errES := esr.CreateArticle(context.Background(), article)
	if errES != nil {
		return domain.Article{}, errES
	}
	errRedis := car.CreateArticleCache(article)
	if errRedis != nil {
		return domain.Article{}, errRedis
	}

	// fmt.Println(service.Query.GetPublishedArticles())
	return article, nil
}

//ConvertResultToArticleArray is to convert result to articles
func ConvertResultToArticleArray(result query.QueryResult) []domain.Article {
	var articles []domain.Article
	for _, v := range result.Result.([]domain.Article) {
		articles = append(articles, v)
	}
	return articles
}

//GetPublishedArticlesByCategory is to return articles based on category
func (service *ArticleService) ProfileArticles(userid string, category string, offset int32, limit int32) ([]domain.Article, error) {
	result := service.Query.ProfileArticles(userid, category, limit, offset)
	if result.Error != nil {
		return nil, result.Error
	}
	articles := ConvertResultToArticleArray(result)
	return articles, nil
}

func (service *ArticleService) CreateUserCategory(userid, categoryname, categoryslug string) (domain.CategoryV2, error) {
	category, err := domain.CreateCategoryV2(service, userid, categoryname, categoryslug)
	if err != nil {
		return category, err
	}
	err = service.Repository.SaveCategoryV2(&category)
	if err != nil {
		return domain.CategoryV2{}, err
	}
	return category, nil
}

func (service *ArticleService) CheckUserHasCategoryExist(userid, categoryid string) bool {
	result := service.Query.GetCategoryWithUserid(userid, categoryid)
	return result.Result.(bool)
}

func (service *ArticleService) CheckSlugExists(slug string) bool {
	result := service.Query.GetArticleSlug(slug)
	if result.Error != nil {
		return false
	}
	return true

}

func (service *ArticleService) GetUserCategoriesByUserid(userid string, offset int32, limit int32) ([]domain.CategoryV2, error) {
	result := service.Query.GetCategoriesByUserid(userid, limit, offset)
	if result.Error != nil {
		return []domain.CategoryV2{}, result.Error
	}
	return result.Result.([]domain.CategoryV2), nil
}

func (service *ArticleService) UpdateArticle(articleID, userID, title, slug, body, thumbnail, status string, categories []string) (domain.Article, error) {
	article, err := domain.EditArticle(service, title, slug, body, thumbnail, userID, status, categories...)
	fmt.Println(article)
	if err != nil {
		return domain.Article{}, err
	}

	err = service.Repository.UpdateArticle(articleID, article)
	if err != nil {
		return domain.Article{}, err
	}

	err = service.Repository.UpdateArticleCategories(article.ID, categories)

	return article, nil
}

func (service *ArticleService) DashboardArticles(userID, keyword, category, status string, offset int32, limit int32) ([]domain.Article, error) {
	result := service.Query.DashboardArticles(userID, keyword, category, status, limit, offset)
	if result.Error != nil {
		return nil, result.Error
	}
	articles := ConvertResultToArticleArray(result)
	return articles, nil
}
